
import {
	Ability,
	ArrayExtensions,
	BitsExtensions,
	Courier, Creep,
	DOTA_ABILITY_BEHAVIOR,
	EntityManager,
	GameActivity_t,
	GridNav,
	GridNavCellFlags,
	Hero, item_blink, MathSDK,
	npc_dota_hero_clinkz,
	npc_dota_hero_pangolier, npc_dota_hero_wisp, Rune, Unit, Vector2,
	Vector3,
} from "wrapper/Imports"

import { GeometryX } from "../Geometry/Geometry"
import { MECX } from "../Geometry/MECX"
import { GameX } from "../Helper/GameX"
import { CollisionObjectX } from "./CollisionX/CollisionObjectX"
import CollisionX from "./CollisionX/CollisionX"
import { CollisionTypesX, HitChanceX, SkillshotTypeX } from "./Enum"
import { InputX } from "./InputX"
import { OutputX } from "./OutputX"

export class PredictionX {

	public static PseudoPrediction(unit: Unit, delay: number = 0, forceMovement: boolean = false) {
		if (!forceMovement && (!unit.IsMoving || delay <= 0))
			return unit.Position

		let bonusSpeed = 0
		if (unit instanceof Courier)
			bonusSpeed = ((unit.Level - 1) * 10)

		return unit.InFront(((unit.IdealSpeed) + bonusSpeed) * delay)
	}

	public GetPrediction(
		abil: Ability,
		target: Unit,
		skillShotType: SkillshotTypeX = SkillshotTypeX.Line,
		collisionTypes: CollisionTypesX = CollisionTypesX.AllUnits,
		Delay: number = abil.CastPoint + this.ActivationDelay(abil) + (GameX.Ping / 1000),
		AreaOfEffect: boolean = false,
		AreaOfEffectTargets: Unit[] = [],
	): OutputX {

		return this.PredictionInit(new InputX(
			target,
			abil.Owner!,
			Delay === 0 ? abil.CastPoint + this.ActivationDelay(abil) + (GameX.Ping / 1000) : Delay,
			abil.AOERadius,
			collisionTypes,
			abil.CastRange,
			!abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_NO_TARGET),
			skillShotType,
			abil.Speed,
			abil instanceof item_blink,
			// AreaOfEffect,
			// AreaOfEffectTargets
		))
	}

	private ActivationDelay(abil: Ability) {
		return abil.GetSpecialValue("activation_delay") ?? 0
	}

	private PredictionInit(input: InputX) {

		const simplePrediction = this.GetSimplePrediction(input)

		if (input.AreaOfEffect) {
			this.GetAreaOfEffectPrediction(input, simplePrediction)
		} else if (input.SkillShotType === SkillshotTypeX.AreaOfEffect) {
			simplePrediction.CastPosition = input.Range > 0 ? input.Caster.InFront(input.Range) : input.Caster.Position
		}

		const result = this.GetProperCastPosition(input, simplePrediction)

		if (input.SkillShotType === SkillshotTypeX.Line && !input.AreaOfEffect && input.UseBlink) {
			const tar_pos = result.TargetPosition
			result.BlinkLinePosition = tar_pos.Extend(input.Caster.Position.Subtract(tar_pos), 200)
			result.CastPosition = result.TargetPosition
		}

		if (!this.CheckRange(input, result))
			return result

		const result2 = this.CheckCollision(input, result)

		return result2
	}

	private TurnTime(unit: Unit, angle: number | Vector3) {
		if (angle instanceof Vector3)
			angle = unit.FindRotationAngle(angle)

		if (unit instanceof npc_dota_hero_wisp || unit instanceof npc_dota_hero_pangolier || unit instanceof npc_dota_hero_clinkz)
			return 0

		if (angle <= 0.2)
			return 0

		return (0.03 / unit.TurnRate(true)) * angle
	}

	private GetSimplePrediction(input: InputX) {

		const predictionOutput = new OutputX()

		let num = input.Delay
		const caster = input.Caster
		const target = input.Target
		const position = input.Target.Position

		predictionOutput.Target = target

		if (target === caster) {
			predictionOutput.HitChance = HitChanceX.High
			predictionOutput.TargetPosition = position
			predictionOutput.CastPosition = position
			return predictionOutput
		}

		if (input.RequiresToTurn)
			num += this.TurnTime(caster, position)

		if (input.Speed > 0)
			num += caster.Distance(position, true) / input.Speed

		let totalDelay = (GameX.Ping / 1000) + 0.06
		let totalArrivalTime = totalDelay + num
		const targetPosition = target.IsMoving ? this.GetPredictedPosition(target, num) : target.Position

		if (target.NetworkActivity === GameActivity_t.ACT_DOTA_RUN) {
			const rotation = MathSDK.DegreesToRadian(target.RotationDifference)
			const direction = (rotation !== 0)
				? Vector3.FromVector2(Vector2.FromPolarCoordinates(rotation, 1).Rotated(rotation))
				: Vector3.FromVector2(Vector2.FromPolarCoordinates(rotation, 1))

			if (rotation !== 0) {
				const timeToRotate = target.TurnTime(Math.abs(rotation));
				totalDelay -= timeToRotate;
				totalArrivalTime -= timeToRotate;
			}
			if (input.Speed !== Number.MAX_VALUE) {
				const result = GeometryX.VectorMovementCollision(
					Vector2.FromVector3(targetPosition),
					Vector2.FromVector3(this.ExtendUntilWall(targetPosition, direction, totalArrivalTime * target.IdealSpeed)),
					target.IdealSpeed, Vector2.FromVector3(caster.Position), input.Speed, totalDelay,
				)
				if (!(result[1] as Vector2).IsZero())
					totalArrivalTime = (result[0] as number) + totalDelay
			}
		}

		predictionOutput.TargetPosition = targetPosition
		predictionOutput.CastPosition = targetPosition

		if (!target.IsVisible)
			predictionOutput.HitChance = HitChanceX.Low
		else if (target.IsStunned || target.IsRooted)
			predictionOutput.HitChance = HitChanceX.Immobile
		else if (target.NetworkActivity !== GameActivity_t.ACT_DOTA_RUN && !caster.IsVisibleForEnemies)
			predictionOutput.HitChance = HitChanceX.High
		else
			predictionOutput.HitChance = ((totalArrivalTime > 0.5) ? HitChanceX.Medium : HitChanceX.High)

		return predictionOutput
	}

	private GetProperCastPosition(input: InputX, output: OutputX) {

		if (input.SkillShotType === SkillshotTypeX.RangedAreaOfEffect || input.SkillShotType === SkillshotTypeX.AreaOfEffect)
			return output

		if (input.SkillShotType === SkillshotTypeX.Line && input.UseBlink)
			return output

		const radius = input.Radius

		if (radius <= 0)
			return output

		const position = input.Caster.Position
		let castPosition = output.CastPosition
		let	num = position.Distance(castPosition)
		const castRange = input.Range

		if (castRange >= num)
			return output

		castPosition.Extend(input.Caster.Position.Subtract(castPosition), Math.min(num - castRange, radius))

		if (output.AoeTargetsHit.length > 1) {
			const num2 = Math.max(...output.AoeTargetsHit.map(x => x.TargetPosition.Distance2D(castPosition)))
			if (num2 > radius) {
				num = position.Distance2D(castPosition);
				castPosition = position.Extend2D(castPosition, num + (num2 - radius));
			}
		}

		output.CastPosition = castPosition;

		return output
	}

	private CheckRange(input: InputX, output: OutputX): boolean {
		if (input.Radius >= 999999 || input.Range >= 999999) {
			return true
		}
		if (input.SkillShotType === SkillshotTypeX.AreaOfEffect) {
			if (output.TargetPosition.Distance2D(output.CastPosition) > input.Radius) {
				output.HitChance = HitChanceX.Impossible
				return false
			}
			return true
		} else if (input.UseBlink && input.SkillShotType === SkillshotTypeX.Line) {
			if (input.Caster.Distance(output.CastPosition) > input.Range + input.Range) {
				output.HitChance = HitChanceX.Impossible
				return false
			}
			return true
		} else {
			if (input.Caster.Distance(output.CastPosition) > input.Range && (input.SkillShotType === SkillshotTypeX.RangedAreaOfEffect
				|| input.Caster.Distance(output.TargetPosition) > input.Range)) {
				output.HitChance = HitChanceX.Impossible
				return false
			}
			return true
		}
	}

	private CheckCollision(input: InputX, output: OutputX) {
		if (input.CollisionTypes === CollisionTypesX.None)
			return output

		const list: Unit[] = []
		const list2: CollisionObjectX[] = []

		const caster = input.Caster
		const scanRange = caster.Distance(output.CastPosition)
		const source = [...EntityManager.GetEntitiesByClass(Hero), ...EntityManager.GetEntitiesByClass(Creep)].filter(x =>
			x !== caster
			&& x !== input.Target
			&& x.IsAlive
			&& x.IsVisible
			&& x.Distance2D(caster) < scanRange,
		)

		if (BitsExtensions.HasMask(input.CollisionTypes, CollisionTypesX.AllyCreeps))
			source.map(x => x instanceof Creep && !x.IsEnemy(caster) && list.push(x))

		if (BitsExtensions.HasMask(input.CollisionTypes, CollisionTypesX.EnemyCreeps))
			source.map(x => x instanceof Creep && x.IsEnemy(caster) && list.push(x))

		if (BitsExtensions.HasMask(input.CollisionTypes, CollisionTypesX.AllyHeroes))
			source.map(x => x instanceof Hero && !x.IsEnemy(caster) && list.push(x))

		if (BitsExtensions.HasMask(input.CollisionTypes, CollisionTypesX.EnemyHeroes))
			source.map(x => x instanceof Hero && x.IsEnemy(caster) && list.push(x))

		if (BitsExtensions.HasMask(input.CollisionTypes, CollisionTypesX.Runes)) {
			EntityManager.GetEntitiesByClass(Rune).filter(x => input.Caster.IsInRange(x, scanRange)).map(rune => {
				list2.push(new CollisionObjectX(rune, Vector2.FromVector3(rune.Position), 75))
			})
		}

		list.forEach(unit => {
			const input2 = new InputX(
				unit,
				input.Caster,
				input.Delay,
				input.Radius,
				input.CollisionTypes,
				input.Range,
				input.RequiresToTurn,
				input.SkillShotType,
				input.Speed,
				input.UseBlink,
				input.AreaOfEffect,
				input.AreaOfEffectTargets,
			)

			list2.push(new CollisionObjectX(unit, Vector2.FromVector3(this.GetSimplePrediction(input2).TargetPosition), unit.CollisionRadius))
		})

		if (CollisionX.GetCollision(Vector2.FromVector3(caster.Position), Vector2.FromVector3(output.CastPosition), input.Radius, list2).Collides)
			output.HitChance = HitChanceX.Impossible

		return output
	}

	private ExtendUntilWall(start: Vector3, direction: Vector3, distance: number) {
		if (GridNav === undefined)
			return start
		let step = 32
		const testPoint = start
		const sign = (distance > 0) ? 1 : -1

		while (distance > 0 && (BitsExtensions.HasBit(GridNav.GetCellFlagsForPos(Vector2.FromVector3(testPoint)), GridNavCellFlags.Walkable))) {
			if (step > distance)
				step = distance

			if (!BitsExtensions.HasBit(GridNav.GetCellFlagsForPos(Vector2.FromVector3(testPoint)), GridNavCellFlags.Tree)) {
				testPoint.AddScalarForThis(sign).Multiply(direction).MultiplyScalar(step)
			} else {
				testPoint.AddScalarForThis(step).Multiply(direction.MultiplyScalar(step))
			}
			distance -= step
		}
		return testPoint
	}

	private GetAreaOfEffectPrediction(input: InputX, output: OutputX) {
		const targets: OutputX[] =  []
		switch (input.SkillShotType) {
			case SkillshotTypeX.Circle: {
				targets.unshift(output)
				if (targets.length === 1) {
					output.AoeTargetsHit.push(output);
				} else {
					while (targets.length > 1) {
						const mecResult = MECX.GetMec(targets.map(x => Vector2.FromVector3(x.TargetPosition)))
						if (mecResult.radius > 0 && mecResult.radius < input.Radius && input.Caster.Distance(Vector3.FromVector2(mecResult.center)) < input.Range) {
							const v2 = targets.length <= 2
								? Vector2.FromVector3(targets[0].TargetPosition).Add(Vector2.FromVector3(targets[1].TargetPosition).DivideScalar(2))
								: mecResult.center
							output.CastPosition = new Vector3(v2.x, v2.y, output.CastPosition.z)
							output.AoeTargetsHit = targets.filter(x => output.CastPosition.IsInRange(x.TargetPosition, input.Radius))
							break;
						}
						const itemToRemove = targets.find(x => targets[0].TargetPosition.DistanceSqr(x.TargetPosition))
						ArrayExtensions.arrayRemove(targets, itemToRemove)
						output.AoeTargetsHit.push(output);
					}
				}
			}
			// tslint:disable-next-line: indent
			                            break;
		}
	}

	private GetPredictedPosition(unit: Unit, delay: number = 0, forceMovement: boolean = false) {
		if (!forceMovement && ((!unit.IsMoving && !unit.ModifiersBook.HasBuffByName("modifier_spirit_breaker_charge_of_darkness")) || delay <= 0))
			return unit.Position
		const networkRotationRad = unit.RotationRad
		const value = new Vector3(Math.cos(networkRotationRad), Math.sin(networkRotationRad), 0)
		return unit.Position.Add(value.MultiplyScalar(delay * unit.IdealSpeed))
	}
}
