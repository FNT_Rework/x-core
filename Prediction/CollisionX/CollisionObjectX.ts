import { Entity, Unit, Vector2 } from "wrapper/Imports"

export class CollisionObjectX {
	constructor(
		public entity: Unit | Entity,
		public Position = Vector2.FromVector3(entity.Position),
		public Radius = Entity instanceof Unit ? Entity.HullRadius : 0,
	) { }
}
