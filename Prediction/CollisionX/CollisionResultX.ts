import { CollisionObjectX } from "./CollisionObjectX"

export class CollisionResultX {
	constructor(
		public readonly CollisionObjects: CollisionObjectX[],
	) { }

	public get Collides() {
		return this.CollisionObjects.length > 0
	}
}
