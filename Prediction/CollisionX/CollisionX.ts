import { ArrayExtensions, Vector2 } from "wrapper/Imports"
import { CollisionObjectX } from "./CollisionObjectX"
import { CollisionResultX } from "./CollisionResultX"

export default new (class CollisionX {
	public GetCollision(startPosition: Vector2, endPosition: Vector2, radius: number, collisionObjects: CollisionObjectX[]): CollisionResultX {
		return new CollisionResultX(ArrayExtensions.orderBy(
			collisionObjects.filter(obj => obj.Position.DistanceSegment(startPosition, endPosition, true) < radius + obj.Radius + obj.entity.CollisionRadius),
			obj => obj.Position.DistanceSqr(startPosition),
		))
	}
})()
