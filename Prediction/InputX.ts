import { Unit } from "wrapper/Imports"
import { CollisionTypesX, SkillshotTypeX } from "./Enum"

export class InputX {
	constructor(
		public Target: Unit,
		public Caster: Unit,
		public Delay: number,
		public Radius: number,
		public CollisionTypes: CollisionTypesX,
		public Range: number,
		public RequiresToTurn: boolean,
		public SkillShotType: SkillshotTypeX,
		public Speed: number,
		public UseBlink: boolean,
		public AreaOfEffect: boolean = false,
		public AreaOfEffectTargets: Unit[] = [],
	) { }
}
