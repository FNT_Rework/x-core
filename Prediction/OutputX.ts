import { Unit, Vector3 } from "wrapper/Imports"
import { HitChanceX } from "./Enum"

export class OutputX {
	public Target?: Unit
	public CastPosition: Vector3 = new Vector3()
	public TargetPosition: Vector3 = new Vector3()
	public HitChance: HitChanceX = HitChanceX.Impossible
	public BlinkLinePosition: Vector3 = new Vector3()
	public AoeTargetsHit: OutputX[] = []
}
