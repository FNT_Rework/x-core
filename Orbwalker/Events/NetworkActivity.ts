import { EventsSDK } from "wrapper/Imports";
import { GameX, OrbwalkerMapX } from "../../Imports";

EventsSDK.on("NetworkActivityChanged", unit => {
	if (!GameX.IsInGame)
		return
	const Orbwalker = OrbwalkerMapX.get(unit)
	if (Orbwalker === undefined)
		return
	Orbwalker.OnNetworkActivity(unit)
})
