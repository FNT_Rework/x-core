import { EventsSDK, Hero, SpiritBear } from "wrapper/Imports"
import { OrbwalkerMapX, OrbwalkerX } from "../../Imports"

EventsSDK.on("EntityCreated", ent => {
	if (ent instanceof Hero || ent instanceof SpiritBear)
		OrbwalkerMapX.set(ent, new OrbwalkerX(ent))
})

EventsSDK.on("EntityDestroyed", ent => {
	if (ent instanceof Hero ||  ent instanceof SpiritBear)
		OrbwalkerMapX.delete(ent)
})
