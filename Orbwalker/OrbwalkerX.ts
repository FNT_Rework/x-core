import { Ability, DOTAUnitAttackCapability_t, dotaunitorder_t, DOTA_ABILITY_BEHAVIOR, ExecuteOrder, GameActivity_t, GameSleeper, GameState, Input, Item, Menu, modifierstate, SPELL_IMMUNITY_TYPES, Unit, Vector3 } from "wrapper/Imports"
import { GameX } from "../Helper/GameX"

export interface OrbwalkerSettings {
	TurnDelay: Menu.Slider
	MoveDelay: Menu.Slider
	HoldRange: Menu.Slider
	AttackDelay: Menu.Slider
}

export class OrbwalkerX {

	public static Sleeper = new GameSleeper()

	public readonly attackActivities: GameActivity_t[] = [
		GameActivity_t.ACT_DOTA_ATTACK,
		GameActivity_t.ACT_DOTA_ATTACK2,
		GameActivity_t.ACT_DOTA_ATTACK_EVENT,
	]

	public readonly attackCancelActivities: GameActivity_t[] = [
		GameActivity_t.ACT_DOTA_IDLE,
		GameActivity_t.ACT_DOTA_IDLE_RARE,
		GameActivity_t.ACT_DOTA_RUN,
	]

	public OrbwalkingPoint = new Vector3()

	private TurnEndTime: number = 0
	private EchoSabre: Nullable<Item>
	private LastAttackTime: number = 0
	private LastMoveOrderIssuedTime: number = 0
	private LastAttackOrderIssuedTime: number = 0
	private Ability: Nullable<Ability> = undefined

	constructor(public Owner: Unit) { }

	public get PingTime() {
		return GameX.Ping / 2000
	}

	public CanMove(time: number = GameState.RawGameTime) {
		return (((time - 0.1) + this.PingTime) - this.LastAttackTime) > this.Owner.AttackPoint
	}

	public GetTurnTime(unit: Unit, menu: OrbwalkerSettings, time: number = GameState.RawGameTime) {
		return time + this.PingTime + this.Owner.TurnTime(unit.Position) + (menu.TurnDelay.value / 1000)
	}

	public Move(position: Vector3, menu: OrbwalkerSettings, time: number = GameState.RawGameTime) {
		if (this.Owner.IsInAbilityPhase)
			return false

		if (this.Owner.Position.Distance(position) < menu.HoldRange.value)
			return false

		if ((time - this.LastMoveOrderIssuedTime) < (menu.MoveDelay.value / 1000))
			return false

		this.Owner.MoveTo(position)
		this.LastMoveOrderIssuedTime = time
		return true
	}

	public CanAttack(target: Unit, menu: OrbwalkerSettings, time: number = GameState.RawGameTime) {
		return this.OrbwalkCanAttack(this.Owner)
			&& (this.GetTurnTime(target, menu, time) - this.LastAttackTime) > (1 / this.Owner.AttacksPerSecond)
			&& !this.Owner.UnitState.some(x => (x & 524288) || x === modifierstate.MODIFIER_STATE_ATTACK_IMMUNE)
			&& !target.IsInvulnerable && !target.IsEthereal
	}

	public OrbwalkTo(target: Nullable<Unit>, menu: OrbwalkerSettings) {
		const time = GameState.RawGameTime

		if (this.TurnEndTime > time || !this.Owner.IsControllable)
			return false

		if (this.Owner.IsChanneling || this.Owner.IsIllusion || this.Owner.IsInAbilityPhase || !this.Owner.IsAlive || this.Owner.IsStunned)
			return false

		if ((target === undefined || !this.CanAttack(target, menu)) && this.CanMove(time)) {

			if (!this.OrbwalkingPoint.IsZero())
				return this.Move(this.OrbwalkingPoint, menu, time)

			return this.Move(Input.CursorOnWorld, menu, time)
		}

		if (target !== undefined && this.CanAttack(target, menu))
			return this.Attack(target, menu, time)

		return false
	}

	public OnNetworkActivity(sender: Unit) {
		if (sender !== this.Owner || sender.IsIllusion)
			return
		const newNetworkActivity = sender.NetworkActivity
		if (this.attackActivities.includes(newNetworkActivity)) {
			if (this.EchoSabre?.IsValid && Math.abs(this.EchoSabre.Cooldown) < 0.15)
				return
			this.LastAttackTime = GameState.RawGameTime - this.PingTime
		} else if (this.attackCancelActivities.includes(newNetworkActivity)) {
			if (!this.CanMove(GameState.RawGameTime + 0.05))
				this.LastAttackTime = 0
		}
	}

	public OnExecuteOrder(order: ExecuteOrder, menu?: Nullable<OrbwalkerSettings>) {
		if (menu === undefined
			|| !(order.Issuers[0] instanceof Unit)
			|| order.Issuers[0] !== this.Owner
			|| order.Issuers[0].IsIllusion
			|| !(order.Target instanceof Unit)
			|| order.Queue
			|| order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_ATTACK_TARGET)
			return

		const target = order.Target
		if (target === undefined || !target.IsValid)
			return

		if (this.CanMove())
			this.LastAttackTime = this.GetTurnTime(target, menu) - this.PingTime
	}

	public Attack(unit: Unit, menu: OrbwalkerSettings, time: number = GameState.RawGameTime) {
		if (this.Owner.IsInAbilityPhase || unit.IsInvulnerable || (time - this.LastAttackOrderIssuedTime) < (menu.AttackDelay.value / 1000))
			return false

		const ability = this.GetOrbSpell(unit)
		if (OrbwalkerX.Sleeper.Sleeping(`X_Core_Orbwalk_${this.Owner.Index}`))
			return false

		this.TurnEndTime = this.GetTurnTime(unit, menu, time)

		ability === undefined || !ability.CanBeCasted()
			? this.Owner.AttackTarget(unit)
			: this.Owner.CastTarget(ability, unit)

		this.LastAttackOrderIssuedTime = time
		OrbwalkerX.Sleeper.Sleep((GameX.Ping / 1000) + 150, `X_Core_Orbwalk_${this.Owner.Index}`)
		return true
	}

	private GetOrbSpell(target: Unit): Nullable<Ability> {
		if (this.Owner === undefined || this.Owner.IsIllusion)
			return undefined

		this.Ability = this.Owner.Spells.find(abil => abil !== undefined && !abil.IsHidden && !abil.IsPassive
			&& abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_UNIT_TARGET)
			&& abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_AUTOCAST)
			&& abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_ATTACK))

		if (this.Ability === undefined)
			return undefined

		if (this.Ability.AbilityImmunityType === SPELL_IMMUNITY_TYPES.SPELL_IMMUNITY_ENEMIES_NO && target.IsMagicImmune)
			this.Ability = undefined

		return this.Ability
	}

	private OrbwalkCanAttack(source: Unit) {
		return source.AttackCapabilities !== DOTAUnitAttackCapability_t.DOTA_UNIT_CAP_NO_ATTACK
			&& !source.IsDisarmed
	}
}

export const OrbwalkerMapX = new Map<Unit, OrbwalkerX>()
