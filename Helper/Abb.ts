import { Hero, npc_dota_hero_abaddon, npc_dota_hero_abyssal_underlord, npc_dota_hero_alchemist, npc_dota_hero_ancient_apparition, npc_dota_hero_antimage, npc_dota_hero_arc_warden, npc_dota_hero_axe, npc_dota_hero_bane, npc_dota_hero_batrider, npc_dota_hero_beastmaster, npc_dota_hero_bloodseeker, npc_dota_hero_bounty_hunter, npc_dota_hero_brewmaster, npc_dota_hero_bristleback, npc_dota_hero_broodmother, npc_dota_hero_centaur, npc_dota_hero_chaos_knight, npc_dota_hero_chen, npc_dota_hero_clinkz, npc_dota_hero_crystal_maiden, npc_dota_hero_dark_seer, npc_dota_hero_dark_willow, npc_dota_hero_dazzle, npc_dota_hero_death_prophet, npc_dota_hero_disruptor, npc_dota_hero_doom_bringer, npc_dota_hero_dragon_knight, npc_dota_hero_drow_ranger, npc_dota_hero_earthshaker, npc_dota_hero_earth_spirit, npc_dota_hero_elder_titan, npc_dota_hero_ember_spirit, npc_dota_hero_enchantress, npc_dota_hero_enigma, npc_dota_hero_faceless_void, npc_dota_hero_furion, npc_dota_hero_grimstroke, npc_dota_hero_gyrocopter, npc_dota_hero_hoodwink, npc_dota_hero_huskar, npc_dota_hero_invoker, npc_dota_hero_jakiro, npc_dota_hero_juggernaut, npc_dota_hero_keeper_of_the_light, npc_dota_hero_kunkka, npc_dota_hero_legion_commander, npc_dota_hero_leshrac, npc_dota_hero_lich, npc_dota_hero_life_stealer, npc_dota_hero_lina, npc_dota_hero_lion, npc_dota_hero_lone_druid, npc_dota_hero_luna, npc_dota_hero_lycan, npc_dota_hero_magnataur, npc_dota_hero_mars, npc_dota_hero_medusa, npc_dota_hero_meepo, npc_dota_hero_mirana, npc_dota_hero_monkey_king, npc_dota_hero_morphling, npc_dota_hero_naga_siren, npc_dota_hero_necrolyte, npc_dota_hero_nevermore, npc_dota_hero_night_stalker, npc_dota_hero_nyx_assassin, npc_dota_hero_obsidian_destroyer, npc_dota_hero_ogre_magi, npc_dota_hero_omniknight, npc_dota_hero_oracle, npc_dota_hero_pangolier, npc_dota_hero_phantom_assassin, npc_dota_hero_phantom_lancer, npc_dota_hero_phoenix, npc_dota_hero_puck, npc_dota_hero_pudge, npc_dota_hero_pugna, npc_dota_hero_queenofpain, npc_dota_hero_rattletrap, npc_dota_hero_razor, npc_dota_hero_riki, npc_dota_hero_rubick, npc_dota_hero_sand_king, npc_dota_hero_shadow_demon, npc_dota_hero_shadow_shaman, npc_dota_hero_shredder, npc_dota_hero_silencer, npc_dota_hero_skeleton_king, npc_dota_hero_slardar, npc_dota_hero_slark, npc_dota_hero_snapfire, npc_dota_hero_sniper, npc_dota_hero_spectre, npc_dota_hero_spirit_breaker, npc_dota_hero_storm_spirit, npc_dota_hero_sven, npc_dota_hero_techies, npc_dota_hero_templar_assassin, npc_dota_hero_terrorblade, npc_dota_hero_tidehunter, npc_dota_hero_tinker, npc_dota_hero_tiny, npc_dota_hero_treant, npc_dota_hero_troll_warlord, npc_dota_hero_tusk, npc_dota_hero_undying, npc_dota_hero_ursa, npc_dota_hero_vengefulspirit, npc_dota_hero_venomancer, npc_dota_hero_viper, npc_dota_hero_visage, npc_dota_hero_void_spirit, npc_dota_hero_warlock, npc_dota_hero_weaver, npc_dota_hero_windrunner, npc_dota_hero_winter_wyvern, npc_dota_hero_wisp, npc_dota_hero_witch_doctor, npc_dota_hero_zuus } from "wrapper/Imports"

export class AbbX {

	public static NAME_HERO(hero: Hero, IsRU: boolean = false) {

		let name = ""
		if (hero instanceof npc_dota_hero_abaddon)
			name = IsRU ? "абадон" : "abaddon"

		if (hero instanceof npc_dota_hero_alchemist)
			name = IsRU ? "алхим" : "alchemist"

		if (hero instanceof npc_dota_hero_ancient_apparition)
			name = IsRU ? "апарат" : "AA"

		if (hero instanceof npc_dota_hero_antimage)
			name = IsRU ? "магина" : "antimage"

		if (hero instanceof npc_dota_hero_arc_warden)
			name = IsRU ? "арк" : "arc warden"

		if (hero instanceof npc_dota_hero_axe)
			name = IsRU ? "акс" : "axe"

		if (hero instanceof npc_dota_hero_bane)
			name = IsRU ? "бейн" : "bane"

		if (hero instanceof npc_dota_hero_batrider)
			name = IsRU ? "бетрайдер" : "batrider"

		if (hero instanceof npc_dota_hero_beastmaster)
			name = IsRU ? "бист" : "beast"

		if (hero instanceof npc_dota_hero_bloodseeker)
			name = IsRU ? "блудсикер" : "bloodseeker"

		if (hero instanceof npc_dota_hero_bounty_hunter)
			name = IsRU ? "баунти хантер" : "bounty hunter"

		if (hero instanceof npc_dota_hero_brewmaster)
			name = IsRU ? "брюмастер" : "brewmaster"

		if (hero instanceof npc_dota_hero_bristleback)
			name = IsRU ? "брист" : "brist"

		if (hero instanceof npc_dota_hero_broodmother)
			name = IsRU ? "бруда" : "brood"

		if (hero instanceof npc_dota_hero_centaur)
			name = IsRU ? "кентавр" : "centaur"

		if (hero instanceof npc_dota_hero_chaos_knight)
			name = IsRU ? "цк" : "CK"

		if (hero instanceof npc_dota_hero_chen)
			name = IsRU ? "чен" : "chen"

		if (hero instanceof npc_dota_hero_clinkz)
			name = IsRU ? "боник" : "clinkz"

		if (hero instanceof npc_dota_hero_rattletrap)
			name = IsRU ? "клок" : "clock"

		if (hero instanceof npc_dota_hero_crystal_maiden)
			name = IsRU ? "цмка" : "cm"

		if (hero instanceof npc_dota_hero_dark_seer)
			name = IsRU ? "дарк сир" : "dark seer"

		if (hero instanceof npc_dota_hero_dark_willow)
			name = IsRU ? "мереска" : "willow"

		if (hero instanceof npc_dota_hero_dazzle)
			name = IsRU ? "дазл" : "dazzle"

		if (hero instanceof npc_dota_hero_death_prophet)
			name = IsRU ? "банша" : "banshee"

		if (hero instanceof npc_dota_hero_disruptor)
			name = IsRU ? "диз" : "disruptor"

		if (hero instanceof npc_dota_hero_doom_bringer)
			name = IsRU ? "дум" : "doom"

		if (hero instanceof npc_dota_hero_dragon_knight)
			name = IsRU ? "дк" : "dc"

		if (hero instanceof npc_dota_hero_drow_ranger)
			name = IsRU ? "дровка" : "drow"

		if (hero instanceof npc_dota_hero_earth_spirit)
			name = IsRU ? "земля" : "earth spirit"

		if (hero instanceof npc_dota_hero_earthshaker)
			name = IsRU ? "шейкер" : "shaker"

		if (hero instanceof npc_dota_hero_elder_titan)
			name = IsRU ? "титан" : "titan"

		if (hero instanceof npc_dota_hero_ember_spirit)
			name = IsRU ? "эмбер" : "ember"

		if (hero instanceof npc_dota_hero_enchantress)
			name = IsRU ? "энча" : "encha"

		if (hero instanceof npc_dota_hero_enigma)
			name = IsRU ? "энигма" : "enigma"

		if (hero instanceof npc_dota_hero_faceless_void || hero instanceof npc_dota_hero_void_spirit)
			name = IsRU ? "воид" : "void"

		if (hero instanceof npc_dota_hero_grimstroke)
			name = IsRU ? "грим" : "grim"

		if (hero instanceof npc_dota_hero_gyrocopter)
			name = IsRU ? "гиро" : "gyro"

		if (hero instanceof npc_dota_hero_huskar)
			name = IsRU ? "хуск" : "huskar"

		if (hero instanceof npc_dota_hero_hoodwink)
			name = IsRU ? "белка" : "hoodwink"

		if (hero instanceof npc_dota_hero_invoker)
			name = IsRU ? "инвокер" : "invo"

		if (hero instanceof npc_dota_hero_wisp)
			name = IsRU ? "висп" : "wisp"

		if (hero instanceof npc_dota_hero_jakiro)
			name = IsRU ? "тхд" : "thd"

		if (hero instanceof npc_dota_hero_juggernaut)
			name = IsRU ? "джагер" : "jugger"

		if (hero instanceof npc_dota_hero_keeper_of_the_light)
			name = IsRU ? "котёл" : "kotl"

		if (hero instanceof npc_dota_hero_kunkka)
			name = IsRU ? "кунка" : "kunkka"

		if (hero instanceof npc_dota_hero_legion_commander)
			name = IsRU ? "лега" : "legion"

		if (hero instanceof npc_dota_hero_leshrac)
			name = IsRU ? "леший" : "leshrac"

		if (hero instanceof npc_dota_hero_lich)
			name = IsRU ? "лич" : "lich"

		if (hero instanceof npc_dota_hero_life_stealer)
			name = IsRU ? "гуля" : "life stealer"

		if (hero instanceof npc_dota_hero_lina)
			name = IsRU ? "лина" : "lina"

		if (hero instanceof npc_dota_hero_lion)
			name = IsRU ? "лион" : "lion"

		if (hero instanceof npc_dota_hero_lone_druid)
			name = IsRU ? "друид" : "druid"

		if (hero instanceof npc_dota_hero_luna)
			name = IsRU ? "луна" : "luna"

		if (hero instanceof npc_dota_hero_lycan)
			name = IsRU ? "люкан" : "lycan"

		if (hero instanceof npc_dota_hero_magnataur)
			name = IsRU ? "магнус" : "magnus"

		if (hero instanceof npc_dota_hero_mars)
			name = IsRU ? "марс" : "mars"

		if (hero instanceof npc_dota_hero_medusa)
			name = IsRU ? "медуза" : "medusa"

		if (hero instanceof npc_dota_hero_meepo)
			name = IsRU ? "мипо" : "meepo"

		if (hero instanceof npc_dota_hero_mirana)
			name = IsRU ? "мирана" : "mirana"

		if (hero instanceof npc_dota_hero_monkey_king)
			name = IsRU ? "мк" : "monkey"

		if (hero instanceof npc_dota_hero_morphling)
			name = IsRU ? "морф" : "morph"

		if (hero instanceof npc_dota_hero_naga_siren)
			name = IsRU ? "нага" : "naga"

		if (hero instanceof npc_dota_hero_furion)
			name = IsRU ? "фура" : "furion"

		if (hero instanceof npc_dota_hero_necrolyte)
			name = IsRU ? "некр" : "necrolyte"

		if (hero instanceof npc_dota_hero_night_stalker)
			name = IsRU ? "НС" : "NS"

		if (hero instanceof npc_dota_hero_nyx_assassin)
			name = IsRU ? "нукс" : "nyx"

		if (hero instanceof npc_dota_hero_ogre_magi)
			name = IsRU ? "огр" : "ogre"

		if (hero instanceof npc_dota_hero_omniknight)
			name = IsRU ? "омник" : "omni"

		if (hero instanceof npc_dota_hero_oracle)
			name = IsRU ? "оракл" : "oracle"

		if (hero instanceof npc_dota_hero_obsidian_destroyer)
			name = IsRU ? "од" : "od"

		if (hero instanceof npc_dota_hero_pangolier)
			name = IsRU ? "панго" : "pango"

		if (hero instanceof npc_dota_hero_phantom_assassin)
			name = IsRU ? "фантомка" : "PA"

		if (hero instanceof npc_dota_hero_phantom_lancer)
			name = IsRU ? "лансер" : "PL"

		if (hero instanceof npc_dota_hero_phoenix)
			name = IsRU ? "феникс" : "phoenix"

		if (hero instanceof npc_dota_hero_puck)
			name = IsRU ? "пак" : "puck"

		if (hero instanceof npc_dota_hero_pudge)
			name = IsRU ? "пудж" : "pudge"

		if (hero instanceof npc_dota_hero_pugna)
			name = IsRU ? "пугна" : "pugna"

		if (hero instanceof npc_dota_hero_queenofpain)
			name = IsRU ? "квопа" : "queen"

		if (hero instanceof npc_dota_hero_razor)
			name = IsRU ? "разор" : "razor"

		if (hero instanceof npc_dota_hero_riki)
			name = IsRU ? "рики" : "riki"

		if (hero instanceof npc_dota_hero_rubick)
			name = IsRU ? "рубик" : "rubick"

		if (hero instanceof npc_dota_hero_sand_king)
			name = IsRU ? "скорпион" : "sandking"

		if (hero instanceof npc_dota_hero_shadow_demon)
			name = IsRU ? "ШД" : "shadow demon"

		if (hero instanceof npc_dota_hero_nevermore)
			name = IsRU ? "сф" : "sf"

		if (hero instanceof npc_dota_hero_shadow_shaman)
			name = IsRU ? "шаман" : "shaman"

		if (hero instanceof npc_dota_hero_silencer)
			name = IsRU ? "сайленсер" : "silencer"

		if (hero instanceof npc_dota_hero_silencer)
			name = IsRU ? "скай" : "sky"

		if (hero instanceof npc_dota_hero_slardar)
			name = IsRU ? "слардар" : "slardar"

		if (hero instanceof npc_dota_hero_slark)
			name = IsRU ? "сларк" : "slark"

		if (hero instanceof npc_dota_hero_snapfire)
			name = IsRU ? "бабка" : "snapfire"

		if (hero instanceof npc_dota_hero_sniper)
			name = IsRU ? "снайпер" : "sniper"

		if (hero instanceof npc_dota_hero_spectre)
			name = IsRU ? "спектра" : "spectre"

		if (hero instanceof npc_dota_hero_spirit_breaker)
			name = IsRU ? "бара" : "bara"

		if (hero instanceof npc_dota_hero_storm_spirit)
			name = IsRU ? "шторм" : "storm"

		if (hero instanceof npc_dota_hero_sven)
			name = IsRU ? "свен" : "sven"

		if (hero instanceof npc_dota_hero_techies)
			name = IsRU ? "течис" : "techies"

		if (hero instanceof npc_dota_hero_templar_assassin)
			name = IsRU ? "темпларка" : "templar"

		if (hero instanceof npc_dota_hero_terrorblade)
			name = IsRU ? "террор" : "terror"

		if (hero instanceof npc_dota_hero_tidehunter)
			name = IsRU ? "тайд" : "tide"

		if (hero instanceof npc_dota_hero_shredder)
			name = IsRU ? "тимбер" : "timber"

		if (hero instanceof npc_dota_hero_tinker)
			name = IsRU ? "тинкер" : "tinker"

		if (hero instanceof npc_dota_hero_tiny)
			name = IsRU ? "тини" : "tiny"

		if (hero instanceof npc_dota_hero_treant)
			name = IsRU ? "трент" : "treant"

		if (hero instanceof npc_dota_hero_troll_warlord)
			name = IsRU ? "троль" : "troll"

		if (hero instanceof npc_dota_hero_tusk)
			name = IsRU ? "туск" : "tusk"

		if (hero instanceof npc_dota_hero_abyssal_underlord)
			name = IsRU ? "гусеница" : "underlord"

		if (hero instanceof npc_dota_hero_undying)
			name = IsRU ? "зомби" : "undying"

		if (hero instanceof npc_dota_hero_ursa)
			name = IsRU ? "урса" : "ursa"

		if (hero instanceof npc_dota_hero_vengefulspirit)
			name = IsRU ? "венга" : "venge"

		if (hero instanceof npc_dota_hero_venomancer)
			name = IsRU ? "веник" : "veno"

		if (hero instanceof npc_dota_hero_viper)
			name = IsRU ? "вайпер" : "viper"

		if (hero instanceof npc_dota_hero_visage)
			name = IsRU ? "визаж" : "visage"

		if (hero instanceof npc_dota_hero_warlock)
			name = IsRU ? "варлок" : "warlock"

		if (hero instanceof npc_dota_hero_weaver)
			name = IsRU ? "вивер" : "weaver"

		if (hero instanceof npc_dota_hero_windrunner)
			name = IsRU ? "врка" : "WR"

		if (hero instanceof npc_dota_hero_winter_wyvern)
			name = IsRU ? "виверна" : "ww"

		if (hero instanceof npc_dota_hero_witch_doctor)
			name = IsRU ? "ВД" : "WD"

		if (hero instanceof npc_dota_hero_skeleton_king)
			name = IsRU ? "ВК" : "WK"

		if (hero instanceof npc_dota_hero_zuus)
			name = IsRU ? "зевс" : "zeus"

		return name
	}
}
