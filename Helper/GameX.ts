import { DOTAGameUIState_t, DOTA_GameMode, DOTA_GameState, Flow_t, GameRules, GameState, LocalPlayer, PlayerResource, Team } from "wrapper/Imports"

export class GameX {
	public static get Ping(): number { // don't touch me, that's right.
		return GetLatency(Flow_t.IN) * 1000
	}

	public static get IsInGame() {
		return GameRules !== undefined
			&& LocalPlayer !== undefined
			&& GameRules.IsInGame
			&& GameState.LocalTeam !== Team.Observer
	}

	public static get IsHeroSelection() {
		return GameRules !== undefined
			&& PlayerResource !== undefined
			&& GameState.IsConnected
			&& LocalPlayer !== undefined
			&& !LocalPlayer.IsSpectator
			&& GameState.UIState === DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME
			&& GameRules.GameState !== DOTA_GameState.DOTA_GAMERULES_STATE_POST_GAME
			&& GameRules.GameState !== DOTA_GameState.DOTA_GAMERULES_STATE_TEAM_SHOWCASE
			&& GameRules.GameState !== DOTA_GameState.DOTA_GAMERULES_STATE_WAIT_FOR_PLAYERS_TO_LOAD
			&& (GameRules.GameState === DOTA_GameState.DOTA_GAMERULES_STATE_HERO_SELECTION || !GameRules.IsInGame)
	}

	public static get IsGameEvent() {
		return GameRules !== undefined
			&& GameRules.GameMode === DOTA_GameMode.DOTA_GAMEMODE_EVENT
	}

	public static get IsInProgress() {
		return GameRules !== undefined
			&& GameRules.GameState === DOTA_GameState.DOTA_GAMERULES_STATE_GAME_IN_PROGRESS
	}
}
