import { MapArea, Team } from "wrapper/Imports"

export class LocationX {

	public static MapAreaLocationStr(whereTo: [MapArea, Team]) {
		let str_position = ""
		let str_position_name = "LOCATION"
		switch (whereTo[0]) {
			case MapArea.Top:
				str_position = "TOP"
				break
			case MapArea.Middle:
				str_position = "MID"
				break
			case MapArea.Bottom:
				str_position = "BOT"
				break
			case MapArea.TopJungle:
				str_position = "JUNGLE"
				str_position_name = "TOP"
				break
			case MapArea.BottomJungle:
				str_position = "JUNGLE"
				str_position_name = "BOT"
				break
			case MapArea.RiverTop:
				str_position = "River TOP"
				break
			case MapArea.RiverMiddle:
				str_position = "River MID"
				break
			case MapArea.RiverBottom:
				str_position = "River BOT"
				break
			case MapArea.RoshanPit:
				str_position = "ROSHAN"
				break
			case MapArea.Base:
				str_position = "BASE"
				str_position_name = ""
				break
			default:
				str_position = "UNKNOWN"
				break
		}
		switch (whereTo[1]) {
			case Team.Radiant:
				str_position_name += " Radiant"
				break
			case Team.Dire:
				str_position_name += " Dire"
				break
			default:
				break
		}
		str_position_name = str_position_name.trim()

		return { str_position, str_position_name }
	}
}
