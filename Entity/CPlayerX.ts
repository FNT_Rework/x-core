import DataTeamPlayer from "github.com/octarine-public/wrapper/wrapper/Base/DataTeamPlayer"
import { PlayerX } from "../Data/PlayerX"
import { GameX } from "../Helper/GameX"
import { DOTA_GameMode, GameRules, Hero, item_philosophers_stone } from "../wrapper/Imports"

export class CPlayerX {

	private lastHit = 0
	private denying = 0
	private reliableGold = 0
	private unreliableGold = 690 // TODO solve something with objects

	constructor(
		public owner: Hero,
		public teamData?: DataTeamPlayer,
	) {}

	public get NetWorth() {
		if (this.teamData !== undefined && !this.owner.IsEnemy())
			return this.teamData.NetWorth

		return this.reliableGold + this.unreliableGold
	}

	public set LastHit(value) {
		this.lastHit = value
	}

	public get LastHit() {
		if (this.teamData !== undefined && !this.owner.IsEnemy())
			return this.teamData.LastHitCount

		return this.lastHit
	}

	public set Denying(value) {
		this.denying = value
	}

	public get Denying() {
		if (this.teamData !== undefined && !this.owner.IsEnemy())
			return this.teamData.DenyCount

		return this.denying
	}

	public set ReliableGold(value) {
		this.reliableGold = value
	}

	public get ReliableGold() {
		if (this.teamData !== undefined && !this.owner.IsEnemy())
			return this.teamData.ReliableGold

		return this.reliableGold
	}

	public set UnreliableGold(value) {
		this.unreliableGold = value
	}

	public get UnreliableGold() {
		if (this.teamData !== undefined && !this.owner.IsEnemy())
			return this.teamData.UnreliableGold

		return this.unreliableGold
	}

	public get HasBuyback() {
		if (PlayerX.Sleeper.Sleeping(`PLAYER_X_BUY_BACK_${this.owner.PlayerID}`))
			return false
		if (this.teamData !== undefined && !this.owner.IsEnemy())
			return (this.ReliableGold + this.UnreliableGold) >= this.BuyBackCost(this.NetWorth)
		return (this.ReliableGold + this.UnreliableGold) >= this.BuyBackCost(this.NetWorth)
	}

	/** @internal for X-Core */
	public INIT() {

		if (GameX.IsInProgress)
			this.GoldPerMinute()

		this.GoldTimePthil()
	}

	private GoldTimePthil() {
		if (GameX.IsGameEvent)
			return
		const item = this.owner.Inventory.NeutralItem
		if (!(item instanceof item_philosophers_stone) || PlayerX.Sleeper.Sleeping("XCORE_GOLD_PER_MINUTE_PTHIL_" + this.owner.Index))
			return
		this.ReliableGold += 1
		const Time = GameRules!.GameMode === DOTA_GameMode.DOTA_GAMEMODE_TURBO ? (850 / 2) : 850
		PlayerX.Sleeper.Sleep(Time, "XCORE_GOLD_PER_MINUTE_PTHIL_" + this.owner.Index)
	}

	private GoldPerMinute() {
		if (GameX.IsGameEvent || PlayerX.Sleeper.Sleeping("XCORE_GOLD_PER_MINUTE_" + this.owner.Index))
			return
		this.ReliableGold += 1
		const Time = GameRules!.GameMode === DOTA_GameMode.DOTA_GAMEMODE_TURBO ? (630 / 2) : 630
		PlayerX.Sleeper.Sleep(Time, "XCORE_GOLD_PER_MINUTE_" + this.owner.Index)
	}

	private BuyBackCost(net: number) {
		return Math.floor(200 + net / 13)
	}
}
