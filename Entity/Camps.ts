import NeutralSpawnBox from "github.com/octarine-public/wrapper/wrapper/Base/NeutralSpawnBox"
import { EntityX } from "../Data/Entity"
import { Creep, GameRules, GameState, Hero, LocalPlayer, NeutralSpawnerType, Team, Unit, Vector2, Vector3, WardObserver, WardTrueSight } from "../wrapper/Imports"

export class CampsX {

	public IsAlly = false
	public IsEmpty = false
	public LastAttackTime = 0
	public Owners: Unit[] = []
	public Creeps: Creep[] = []
	public Name: Nullable<string>
	public Position = new Vector3(0, 0)
	public CampType = NeutralSpawnerType.Small

	constructor(name: string, Type: NeutralSpawnerType, position: Vector3, team: Team = Team.Radiant, public Box: Nullable<NeutralSpawnBox>) {
		this.Name = name
		this.CampType = Type
		this.Position = position
		this.IsAlly = LocalPlayer!.Team === team
	}

	public SetEmpty() {
		this.Owners = []
		this.IsEmpty = false
		this.LastAttackTime = 0
	}
}

export class CampManagerX {

	public Camps = EntityX.Camps

	constructor() { /** */ }

	public Init() {
		if (this.Camps.size <= 0) {
			this.SetCamps()
			return
		}
	}

	public Update() {

		const Units = EntityX.AllyUnits.filter(x => x instanceof WardTrueSight
			|| x instanceof WardObserver
			|| x instanceof Hero
			|| (x instanceof Creep && !x.IsLaneCreep && !x.IsNeutral))

		this.Camps.forEach(camp =>
			this.onTick(
				camp,
				Units,
			),
		)
	}

	public Dispose() {
		this.Camps.clear()
	}

	private onTick(camp: CampsX, units: Unit[]) {

		units.forEach(unit => {
			if ((unit instanceof WardTrueSight || unit instanceof WardObserver)
				&& unit.IsAlive && unit.IsVisible && camp.Box !== undefined
				&& camp.Box.Includes2D(Vector2.FromVector3(unit.Position))) {
				camp.IsEmpty = true
				return
			}

			if (!(unit instanceof WardTrueSight || unit instanceof WardObserver) && this.IsValidUnitCamp(unit, camp))
				camp.Owners.push(unit)
		})

		camp.Creeps = EntityX.NeutralCreepX.filter(x => x.Distance2D(camp.Position) < 450)

		if (Math.floor(GameRules!.GameTime % 60) > 0 || (camp.LastAttackTime + 60) > GameState.RawGameTime)
			return

		camp.SetEmpty()
	}

	private SetCamps() {
		if (this.Camps.size !== 0)
			return
		for (var i = EntityX.NeutralSpawnBoxes.length - 1; i >= 0; i--) {
			const spawn = EntityX.NeutralSpawnBoxes[i]
			const setClassData = new CampsX(spawn.Name, spawn.Type, spawn.Position, spawn.SpawnerTeam, spawn.SpawnBox)
			this.Camps.set(spawn.Name, setClassData)
		}
	}

	private IsValidUnitCamp(unit: Unit, camp: CampsX) {
		return unit.IsAlive
			&& unit.IsVisible
			&& !camp.Owners.includes(unit)
			&& unit.Distance2D(camp.Position) < 400
	}
}
