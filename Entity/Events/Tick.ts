import DataTeamPlayer from "github.com/octarine-public/wrapper/wrapper/Base/DataTeamPlayer";
import { EventsSDK, GameState, Hero, npc_dota_hero_meepo, PlayerResource } from "wrapper/Imports";
import { EntityX } from "../../Data/Entity";
import { PlayerX } from "../../Data/PlayerX";
import { GameX } from "../../Helper/GameX";
import { CPlayerX } from "../CPlayerX";

function CacheInitiate(unit: Hero, teamData?: DataTeamPlayer) {
	let cache = PlayerX.DataX.get(unit)
	if (cache === undefined) {
		cache = new CPlayerX(unit, teamData)
		PlayerX.DataX.set(unit, cache)
	}
	cache.INIT()
}

EventsSDK.on("Tick", () => {
	if (!GameX.IsInGame || PlayerResource === undefined)
		return

	if (PlayerX.PauseCooldown <= GameState.RawGameTime)
		PlayerX.CanBeUsePause = true

	const Creeps = [...EntityX.AllyCreeps, ...EntityX.EnemyCreeps]
	const Heroes = [...EntityX.AllyHeroes, ...EntityX.EnemyHeroes]

	EntityX.NeutralCreepX = Creeps.filter(x => x.IsNeutral
		&& !x.IsLaneCreep
		&& !x.IsSummoned,
	)

	Heroes.forEach(unit => {
		if (unit.IsIllusion || (unit instanceof npc_dota_hero_meepo && unit.IsClone))
			return
		if (!unit.IsEnemy()) {
			if (EntityX.TeamDataX === undefined)
				return
			const slot = PlayerResource!.PlayerTeamData[unit.PlayerID]?.TeamSlot
			const teamData = EntityX.TeamDataX.DataTeam[slot]
			if (teamData === undefined)
				return
			CacheInitiate(unit, teamData)
			return
		}
		CacheInitiate(unit)
	})
})
