import { alchemist_goblins_greed, ArrayExtensions, EventsSDK, GameState, NetworkedParticle, npc_dota_hero_alchemist, Unit } from "wrapper/Imports";
import { PlayerX } from "../../Data/PlayerX";
import { GameX } from "../../Helper/GameX";
import { CPlayerX } from "../CPlayerX";

function TAKE_RUNE_ALCH(data: CPlayerX, addGold: number = 40) {
	if (!(data.owner instanceof npc_dota_hero_alchemist)) {
		data.ReliableGold += addGold
		return
	}

	const goblins_greed = data.owner.GetAbilityByClass(alchemist_goblins_greed)
	data.ReliableGold += goblins_greed !== undefined
		? (goblins_greed.Level !== 0 ? (addGold * goblins_greed.GetSpecialValue("bounty_multiplier")) : addGold)
		: (data.ReliableGold += addGold)
}

const particles: NetworkedParticle[] = []
EventsSDK.on("ParticleUpdated", par => {
	if (!GameX.IsInGame || particles.includes(par))
		return
	if (
		!par.Path.includes("particles/generic_gameplay/rune_bounty_owner.vpcf")
		&& !par.Path.includes("particles/items2_fx/hand_of_midas.vpcf")
	)
		return

	let netData: Nullable<CPlayerX>
	for (const cpEnt of par.ControlPointsEnt.values())
		if (cpEnt[0] instanceof Unit) {
			netData = PlayerX.DataX.get(cpEnt[0])
			if (netData === undefined || !netData.owner.IsEnemy())
				netData = undefined
			else
				break
		}
	if (netData === undefined)
		return

	particles.push(par)
	// TODO improve this shit code
	switch (par.Path) {
		case "particles/generic_gameplay/rune_bounty_owner.vpcf":
		if (GameState.RawGameTime < 300) {
			TAKE_RUNE_ALCH(netData)
		} else if (GameState.RawGameTime >= 300) {
			TAKE_RUNE_ALCH(netData, 50)
		} else if (GameState.RawGameTime >= 600) {
			TAKE_RUNE_ALCH(netData, 60)
		} else if (GameState.RawGameTime >= 900) {
			TAKE_RUNE_ALCH(netData, 70)
		} else if (GameState.RawGameTime >= 1200) {
			TAKE_RUNE_ALCH(netData, 80)
		} else if (GameState.RawGameTime >= 1500) {
			TAKE_RUNE_ALCH(netData, 90)
		} else if (GameState.RawGameTime >= 1800) {
			TAKE_RUNE_ALCH(netData, 100)
		} else if (GameState.RawGameTime >= 2100) {
			TAKE_RUNE_ALCH(netData, 110)
		} else if (GameState.RawGameTime >= 2400) {
			TAKE_RUNE_ALCH(netData, 120)
		} else if (GameState.RawGameTime >= 2700) {
			TAKE_RUNE_ALCH(netData, 130)
		} else if (GameState.RawGameTime >= 3000) {
			TAKE_RUNE_ALCH(netData, 140)
		} else if (GameState.RawGameTime >= 3300) {
			TAKE_RUNE_ALCH(netData, 150)
		} else if (GameState.RawGameTime >= 3600) {
			TAKE_RUNE_ALCH(netData, 160)
		} else if (GameState.RawGameTime >= 3900) {
			TAKE_RUNE_ALCH(netData, 170)
		} else if (GameState.RawGameTime >= 4200) {
			TAKE_RUNE_ALCH(netData, 180)
		} else if (GameState.RawGameTime >= 4500) {
			TAKE_RUNE_ALCH(netData, 190)
		} else if (GameState.RawGameTime >= 4800) {
			TAKE_RUNE_ALCH(netData, 200)
		}
		break;
		case "particles/items2_fx/hand_of_midas.vpcf":
			// TODO alch
			netData.UnreliableGold += 160
			break
		}
})

EventsSDK.on("ParticleDestroyed", par => ArrayExtensions.arrayRemove(particles, par))
