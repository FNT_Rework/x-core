import { EventsSDK, GameState } from "wrapper/Imports";
import { PlayerX } from "../../Data/PlayerX";
import { GameX } from "../../Helper/GameX";

EventsSDK.on("PostDataUpdate", () => {
	if (!GameX.IsInGame)
		return
	if (PlayerX.PauseCooldown <= GameState.RawGameTime)
		PlayerX.CanBeUsePause = true
})
