import { DOTA_CHAT_MESSAGE, EntityManager, EventsSDK, GameState, Hero } from "wrapper/Imports"
import { PlayerX } from "../../Data/PlayerX"
import { GameX } from "../../Helper/GameX"

const IsValidMessagePause = (type: DOTA_CHAT_MESSAGE) => {
	return type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_CANTPAUSEYET
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_NOPAUSESLEFT
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_CANTPAUSE
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_PAUSED
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_UNPAUSE_COUNTDOWN
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_AUTO_UNPAUSED
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_YOUPAUSED
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_CANTUNPAUSETEAM
}

const IsValidMessage = (type: DOTA_CHAT_MESSAGE) => {
	return type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_HERO_KILL //  type: 0,  gold: 2001, killer_player_id: 0 1 -1 -1 -1 -1 0 0
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_TOWER_KILL // type: 3, 2 0 - 1 - 1 - 1 - 1 - 1  gold: 90  towerID: 4294967295
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_FIRSTBLOOD // type: 5, gold: 318 killer: 1, who_die: 0 -1 -1 -1 -1 undefined undefined
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_STREAK_KILL // type 6 gold: 191 1 3 2 0 0 -1 undefined undefined
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_BUYBACK // type: 7, calc to netW  | 100 + Networth / 13
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_ROSHAN_KILL // type: 9, gold: 135, ??: 2, palyer_who: 0 -1 -1 -1 -1 undefined undefined
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_COURIER_LOST // need test
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_CANTPAUSEYET
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_NOPAUSESLEFT
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_CANTPAUSE
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_PAUSED
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_UNPAUSE_COUNTDOWN
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_AUTO_UNPAUSED
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_YOUPAUSED
		|| type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_CANTUNPAUSETEAM
}

EventsSDK.on("ChatEvent", (
	type,
	value,
	playerid_1,
	playerid_2,
	playerid_3,
	playerid_4,
	playerid_5,
	playerid_6,
	value2,
	value3,
) => {
	if (!IsValidMessage(type) || !GameX.IsInGame)
		return

	GAME_PAUSE(
		type,
		value,
		playerid_1,
		playerid_2,
	)

	KILL_MESSAGE(
		type,
		value,
		playerid_1,
		playerid_2,
	)

	BUYBACK_MESSAGE(type, playerid_1)
})

function KILL_MESSAGE(
	type: DOTA_CHAT_MESSAGE,
	value: number,
	playerid_1: number,
	playerid_2: number,
) {
	if (type !== DOTA_CHAT_MESSAGE.CHAT_MESSAGE_HERO_KILL && type !== DOTA_CHAT_MESSAGE.CHAT_MESSAGE_FIRSTBLOOD)
		return
	const target = EntityManager.GetEntitiesByClass(Hero).find(x => x.PlayerID === playerid_1)
	const killer = EntityManager.GetEntitiesByClass(Hero).find(x => x.PlayerID === playerid_2)
	if (killer === undefined || target === undefined)
		return
	const DataKiller = PlayerX.DataX.get(killer)
	const DataTarget = PlayerX.DataX.get(target)
	if (DataKiller !== undefined && DataKiller.owner.IsEnemy())
		DataKiller.UnreliableGold += value
	if (DataTarget !== undefined && DataTarget.owner.IsEnemy())
		DataTarget.UnreliableGold -= (DataTarget.NetWorth / 40)
}

function GAME_PAUSE(type: DOTA_CHAT_MESSAGE, value: number, playerid_1: number, playerid_2: number) {

	if (!IsValidMessagePause(type))
		return

	// TODO imroved method pause | add supported team & etc messages on pause limited

	if (type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_CANTPAUSE)
		PlayerX.CanBeUsePause = false

	if (type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_CANTPAUSEYET) {
		PlayerX.CanBeUsePause = false
		PlayerX.PauseCooldown = GameState.RawGameTime + value
	}

	if (type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_PAUSED) {
		PlayerX.CanBeUsePause = false
		PlayerX.PauseCooldown = GameState.RawGameTime + 300
	}
}

function BUYBACK_MESSAGE(type: DOTA_CHAT_MESSAGE, playerid_1: number) {
	if (type !== DOTA_CHAT_MESSAGE.CHAT_MESSAGE_BUYBACK)
		return
	const hero = EntityManager.GetEntitiesByClass(Hero).find(x => x.PlayerID === playerid_1)
	if (hero === undefined || !hero.IsEnemy())
		return
	const BBhero = PlayerX.DataX.get(hero)
	if (BBhero === undefined)
		return
}
