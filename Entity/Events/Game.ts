import { Creep, EntityManager, EventsSDK, GameRules, GameState, Hero, npc_dota_hero_arc_warden, Unit } from "wrapper/Imports"
import { PlayerX } from "../../Data/PlayerX"
import { GameX } from "../../Helper/GameX"
import { CampManagerX } from "../Camps"

const CampManager = new CampManagerX()
EventsSDK.on("GameEvent", (name, obj) => {
	if (!GameX.IsInGame)
		return

	if (name === "dota_buyback")
		PlayerX.Sleeper.Sleep((480 * 1000), `PLAYER_X_BUY_BACK_${obj.player_id}`)

	const target = EntityManager.EntityByIndex(obj.entindex_killed)
	const killer = EntityManager.EntityByIndex(obj.entindex_attacker)

	if (!(killer instanceof Unit) || target instanceof Hero || (target !== undefined && target.Name === "npc_dota_aether_remnant"))
		return

	if (name === "entity_hurt" || name === "entity_killed") {
		CampManager.Camps.forEach(camp => {
			if (!(target instanceof Creep) || !target.IsNeutral || target.IsLaneCreep)
				return
			if (name === "entity_killed" && camp.Position.Distance2D(target.Position) <= 500 && !camp.IsEmpty) {
				if (!camp.Owners.some(x => x === killer))
					camp.Owners.push(killer)
				camp.IsEmpty = true
			}
			if (camp.Position.Distance2D(target.Position) < 300 && Math.floor(GameRules!.GameTime % 60) >= 56) {
				camp.IsEmpty = true
				camp.LastAttackTime = GameState.RawGameTime
			}
		})
	}

	if (name !== "entity_killed")
		return

	const netData = PlayerX.DataX.get(killer)
	if (netData === undefined || !(target instanceof Unit))
		return

	if (!killer.IsEnemy(target)) {
		netData.Denying++
		return
	}

	// TODO npc_dota_observer_wards, Illusions gold
	// if (netData.owner instanceof npc_dota_hero_alchemist) {
	// 	const goblins_greed = netData.owner.GetAbilityByClass(alchemist_goblins_greed) & stackCount buffs
	// }

	if (target instanceof npc_dota_hero_arc_warden && target.IsTempestDouble) {
		if (target.Level === 6)
			netData.UnreliableGold += 180
		if (target.Level === 12)
			netData.UnreliableGold += 240
		if (target.Level === 18)
			netData.UnreliableGold += 300
	}

	if (target.Name.includes("npc_dota_wraith_king_skeleton_warrior"))
		netData.UnreliableGold += 5

	if (PlayerX.UNITS.includes(target.Name))
		netData.UnreliableGold += target.GoldBountyMin

	if (target instanceof Creep)
		netData.UnreliableGold += Math.round(((target.GoldBountyMin + (target.GoldBountyMax)) / 2))

	netData.LastHit++
})
