// import { Ability, ArrayExtensions, EntityManager, EventsSDK, GameSleeper, GameState, Hero, Input, LocalPlayer, Menu, ParticlesSDK, pudge_meat_hook, Unit } from "wrapper/Imports";
// import { GameX, OutputX, PredictionX, } from "./Imports";
// import { HitChanceX } from "./Prediction/Enum";

// const menu = Menu.AddEntryDeep(["Main", "Debugger", "TestHook"])
// const menuState = menu.AddToggle("State")
// const rangeDelay = menu.AddSlider("Closest to mouse", 300, 100, 500)
// const stateDelay = menu.AddSlider("Delay", 100, 0, 500)
// const stateBtn = menu.AddKeybind("Key")
// const particleSDK = new ParticlesSDK()
// const Sleeper = new GameSleeper()

// menuState.OnActivate(() => {
// 	particleSDK.DestroyByKey("PredictionX_T")
// })

// stateBtn.OnRelease(() => {
// 	if (LocalPlayer === undefined || LocalPlayer.Hero === undefined)
// 		return

// 	const abil = LocalPlayer.Hero.GetAbilityByClass(pudge_meat_hook)
// 	if (abil === undefined)
// 		return

// 	LocalPlayer.Hero.OrderStop()
// })

// let rotationTime = 0
// let rotation: number = 0
// let lastTarget: Nullable<Unit>
// const Prediction = new PredictionX()

// function Equals(target: Unit, last: Unit) {
// 	if (last === undefined)
// 		return false
// 	return target.Index === last.Index
// }

// function RotationTime(target: Unit) {
// 	if (!Equals(target, lastTarget!)) {
// 		lastTarget = target
// 		rotationTime = GameState.RawGameTime
// 		rotation = target.RotationRad
// 		return false
// 	}

// 	if (Math.abs(rotation - target.RotationRad) > 0.5) {
// 		rotationTime = GameState.RawGameTime
// 		rotation = target.RotationRad
// 		return false
// 	}
// 	return ((rotationTime + stateDelay.value) / 1000) <= GameState.RawGameTime
// }

// function predictionX(abil: Ability, target: Unit, targets: Unit[]) {
// 	return Prediction.GetPrediction(abil, target)
// }

// EventsSDK.on("Draw", () => {
// 	if (!GameX.IsInGame || !menuState.value || LocalPlayer === undefined || LocalPlayer.Hero === undefined)
// 		return

// 	EntityManager.GetEntitiesByClass(pudge_meat_hook).forEach(abil => {
// 		if (abil === undefined || abil.Owner === undefined)
// 			return

// 		const targets = EntityManager.GetEntitiesByClass(Hero).filter(x => !x.IsIllusion
// 			&& x.IsAlive
// 			&& x.IsVisible
// 			&& x.IsEnemy()
// 		)

// 		let prediction: Nullable<OutputX>
// 		let cancleRotation: Nullable<boolean>

// 		const target = ArrayExtensions.orderBy(targets.filter(x => {

// 			if (x.Distance2D(Input.CursorOnWorld) <= rangeDelay.value) {
// 				prediction = predictionX(abil, x, targets)
// 				cancleRotation = RotationTime(x)
// 				particleSDK.DrawCircle("PredictionX_" + x.Index, LocalPlayer!.Hero!, 80, { Position: prediction.CastPosition })
// 				return true
// 			} else particleSDK.DestroyByKey("PredictionX_" + x.Index)

// 		}), x => x.Distance2D(Input.CursorOnWorld))[0]

// 		if (target === undefined || prediction === undefined || cancleRotation === undefined) {
// 			particleSDK.DestroyByKey("PredictionX_T")
// 			return
// 		}

// 		particleSDK.DrawLineToTarget("PredictionX_T", abil.Owner, target)

// 		if (!stateBtn.is_pressed)
// 			return

// 		if ((!cancleRotation && prediction.HitChance >= HitChanceX.Medium)) {
// 			if (abil.IsInAbilityPhase && !Sleeper.Sleeping("STOP")) {
// 				abil.Owner.OrderStop()
// 				Sleeper.Sleep(50 + (GameX.Ping / 1000), "STOP")
// 			}
// 			return
// 		}

// 		if (!target.IsAlive ||  prediction.HitChance <= HitChanceX.Impossible) {
// 			if (!Sleeper.Sleeping("STOP")) {
// 				abil.Owner.OrderStop()
// 				Sleeper.Sleep(50 + (GameX.Ping / 1000), "STOP")
// 			}
// 			return
// 		}

// 		if (!abil.CanBeCasted() || Sleeper.Sleeping(abil.Index))
// 			return

// 		abil.UseAbility(prediction.TargetPosition)
// 		Sleeper.Sleep((abil.CastPoint * 1000 + 50), abil.Index)
// 	})
// })

// EventsSDK.on("GameEnded", () => {
// 	lastTarget = undefined
// 	rotationTime = 0
// 	rotation = 0
// })
