import { Color, RendererSDK } from "wrapper/Imports"
import { PathX } from "../Data/Path"
import { RectangleX } from "../Geometry/RectangleX"

export class RectangleDraw {

	public static OutlinedRect(rect: RectangleX, width: number = 1, color?: Color) {
		RendererSDK.OutlinedRect(rect.Position, rect.Size, width, color)
	}

	public static FilledRect(rect: RectangleX, color?: Color) {
		RendererSDK.FilledRect(rect.Position, rect.Size, color)
	}

	public static Image(path: string, rect: RectangleX, color?: Color, round: number = -1) {
		RendererSDK.Image(path, rect.Position, round, rect.Size, color)
	}

	public static Radial(
		angle: number = -90,
		cooldown: number,
		cooldownLength: number,
		outlinedRect: boolean,
		rec: RectangleX,
		color: Color = Color.White,
		color2: Color = Color.Green,
	) {

		if (cooldown <= 0 || cooldownLength <= 0)
			return

		if (outlinedRect)
			this.OutlinedRect(rec, 2, color2)

		const timetoshow = (cooldown / cooldownLength)
		RendererSDK.Radial(angle, 100 * timetoshow, rec.Position, rec.Size, color)
	}

	public static Arc(
		angle: number = -90,
		cooldown: number,
		cooldownLength: number,
		outline: boolean = true,
		rec: RectangleX,
		color1: Color = Color.Black,
		color2: Color = Color.Black,
		color3: Color = Color.Green,
		fill_arc: boolean = true,
	) {

		if (cooldown <= 0 || cooldownLength <= 0)
			return

		if (outline)
			RectangleDraw.Image(PathX.Images.buff_outline, rec, color3)

		const cooldown_ratio = 100 * (cooldown / cooldownLength)
		const arc_pos = rec.SubtractSizeVector(5)

		if (fill_arc)
			RendererSDK.Arc(angle, cooldown_ratio, arc_pos.Position, arc_pos.Size, true, 1, color1.SetA(100))

		RendererSDK.Arc(angle, cooldown_ratio, arc_pos.Position, arc_pos.Size, false, 5, color2)
	}

}
