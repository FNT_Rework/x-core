import { Color, Entity, ParticleAttachment_t, ParticlesSDK, Vector3 } from "../wrapper/Imports";

export interface IDrawRectangleOptions {
	Size?: number
	Alpha?: number
	Color?: Color
	PositionEnd?: Vector3
	PositionStart?: Vector3
	Attachment?: ParticleAttachment_t
}

export interface IDrawConeOptions {
	Width?: number
	Color?: Color
	PositionEnd?: Vector3
	PositionStart?: Vector3
	Attachment?: ParticleAttachment_t
}

export interface IDrawTimerOptions {
	Width?: number
	Position?: Vector3
	Attachment?: ParticleAttachment_t
}

export class ParticlesX extends ParticlesSDK {
	public DrawRectangle(
		key: any,
		entity: Entity,
		options: IDrawRectangleOptions = {},
	) {
		return this.AddOrUpdate(key, "gitlab.com/FNT_Rework/X-Core/scripts_files/particles/rectangle/rectangle.vpcf",
			options.Attachment ?? ParticleAttachment_t.PATTACH_ABSORIGIN,
			entity,
			[1, options.PositionStart ?? entity.Position],
			[2, options.PositionEnd ?? entity.Position],
			[3, new Vector3(options.Size ?? 100, 255, options.Alpha ?? 255)],
			[4, options.Color ?? Color.Aqua],
		)
	}

	public DrawConeFinder(
		key: any,
		entity: Entity,
		options: IDrawConeOptions = {},
	) {
		return this.AddOrUpdate(key , "particles/ui_mouseactions/range_finder_cone.vpcf",
			options.Attachment ?? ParticleAttachment_t.PATTACH_ABSORIGIN,
			entity,
			[1, options.PositionStart ?? entity.Position],
			[2, options.PositionEnd ?? entity.Position],
			[3, new Vector3(options.Width ?? 125, options.Width ?? 125, 0)],
			[4, options.Color ?? Color.Aqua],
		)
	}

	public DrawTimerRange(
		key: any,
		entity: Entity,
		options: IDrawTimerOptions = {},
	) {
		return this.AddOrUpdate(key, "particles/units/heroes/hero_snapfire/hero_snapfire_ultimate_calldown.vpcf",
			options.Attachment ?? ParticleAttachment_t.PATTACH_ABSORIGIN,
			entity,
			[0, options.Position ?? entity.Position],
			[1, new Vector3(options.Width ?? 300, 1, -(options.Width ?? 300))],
		)
	}
}
