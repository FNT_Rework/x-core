import { npc_dota_hero_brewmaster, RendererSDK, SpiritBear, Unit, Vector2, Vector3 } from "wrapper/Imports"

export class UnitBarsX {

	private healthBarSize: Vector2 = new Vector2(0, 0)
	private healthBarPositionCorrection: Vector2 = new Vector2(0, 0)

	constructor(public unit: Vector2 | Vector3 | Unit) { }

	public get ScreenSize() {
		return RendererSDK.WindowSize
	}

	public get HealthBarPosition() {

		let offset = 269

		if (this.unit instanceof Unit) {
			// hack
			offset = this.unit.HealthBarOffset
		}

		const screen = RendererSDK.WorldToScreen((
			this.unit instanceof Unit
				? this.unit.Position
				: this.unit as Vector3
		).Clone().AddScalarZ(offset))

		if (screen === undefined)
			return new Vector2(0, 0)

		const position = screen.Subtract(this.HealthBarPositionCorrection)

		// hack
		if (this.unit instanceof SpiritBear) {
			position.AddScalarY(4)
			position.AddScalarX(1)
		}

		if (this.unit instanceof npc_dota_hero_brewmaster) {
			const buffSpirit = this.unit.HasBuffByName("modifier_brewmaster_primal_split")
			if (buffSpirit) {
				position.AddScalarY(9)
				position.AddScalarX(1)
			}
		}

		return position
	}

	public get HealthBarSize() {
		if (this.healthBarSize.IsZero())
			this.healthBarSize = new Vector2(this.ScreenSize.x / 19.2, this.ScreenSize.y / 155.1)

		return this.healthBarSize
	}

	public get HealthBarPositionCorrection() {
		if (this.healthBarPositionCorrection.IsZero())
			this.healthBarPositionCorrection = new Vector2(this.HealthBarSize.x / 1.95, this.ScreenSize.y / 42)

		return this.healthBarPositionCorrection
	}
}
