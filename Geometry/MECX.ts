import { ArrayExtensions, Vector2 } from "wrapper/Imports";
import { RectangleX } from "./RectangleX";

export class MecCircleX {
	constructor(public center: Vector2, public radius: number) {}
}

export class MECX {

	public static GetMec(points: Vector2[]): MecCircleX {
		const ConvexHull = this.MakeConvexHull(points)
		const BC = this.FindMinimalBoundingCircle(ConvexHull, /** out */ new Vector2(0, 0), /** out */ 0)
		return new MecCircleX(BC.centerOS, BC.radiusOS);
	}

	public static MakeConvexHull(points: Vector2[]) {
		points = this.HullCull(points)

		const best_pt = [points[0]]
		points.filter(pt => pt.y < best_pt[0].y || pt.y === best_pt[0].y && pt.x < best_pt[0].x).forEach(pt => { best_pt[0] = pt })

		const hull = [best_pt[0]]
		ArrayExtensions.arrayRemove(points, best_pt[0])

		let sweep_angle = 0

		for (;;) {
			if (points.length === 0)
				break;

			const X = hull[hull.length - 1].x
			const Y = hull[hull.length - 1].y
			best_pt[0] = points[0]
			let best_angle = 3600

			points.forEach(pt => {
				const test_angle = this.AngleValue(X, Y, pt.x, pt.y);
				if (test_angle >= sweep_angle && best_angle > test_angle) {
					best_angle = test_angle;
					best_pt[0] = pt
				}
			})

			const first_angle = this.AngleValue(X, Y, hull[0].x, hull[0].y)

			if (first_angle >= sweep_angle && best_angle >= first_angle)
				break

			hull.push(best_pt[0])
			ArrayExtensions.arrayRemove(points, best_pt[0])
			sweep_angle = best_angle;
		}

		return hull
	}

	public static FindMinimalBoundingCircle(points: Vector2[], center: Vector2, radius: number) {
		const hull = this.MakeConvexHull(points)
		let best_center = points[0]
		let best_radius2 = this.MAX_VALUE;

		for (let i = 0; i < (hull.length - 1); i++) {
			for (let j = i + 1; j < hull.length; j++) {
				const test_center = new Vector2((hull[i].x + hull[j].x) / 2, (hull[i].y + hull[j].y) / 2)
				const dx = test_center.x - hull[i].x
				const dy = test_center.y - hull[i].y
				const test_radius2 = (dx * dx) + (dy * dy)
				if (test_radius2 < best_radius2) {
					if (this.CircleEnclosesPoints(test_center, test_radius2, points, i, j, -1)) {
						best_center = test_center;
						best_radius2 = test_radius2;
					}
				}
			}
		}

		for (let i = 0; i < (hull.length - 2); i++) {
			for (let j = i + 1; j < (hull.length - 1); j++) {
				for (let k = j + 1; k < hull.length; k++) {
					const findCircle = this.FindCircle(hull[i], hull[j], hull[k], new Vector2(), 0);
					if (findCircle.radiusFC < best_radius2) {
						if (this.CircleEnclosesPoints(findCircle.centerFC, findCircle.radiusFC, points, i, j, k)) {
							best_center = findCircle.centerFC
							best_radius2 = findCircle.radiusFC
						}
					}
				}
			}
		}

		center = best_center;
		radius = best_radius2 === this.MAX_VALUE ? 0 : Math.sqrt(best_radius2)

		return {centerOS: center, radiusOS: radius}
	}

	private static MAX_VALUE = 3.40282347e+38 // largest positive number in float32

	private static CircleEnclosesPoints(center: Vector2, radius2: number, points: Vector2[], skip1: number, skip2: number, skip3: number) {
		return points.filter((t, i) => i !== skip1 && i !== skip2 && i !== skip3).map(point => {
			const dx = center.x - point.x
			const dy = center.y - point.y
			return (dx * dx) + (dy * dy)
		}).every(test_radius2 => !(test_radius2 > radius2))
	}

	private static FindCircle(a: Vector2, b: Vector2, c: Vector2, center_: Vector2, radius: number) {
		const x1 = (b.x + a.x) / 2
		const y1 = (b.y + a.y) / 2
		const dy1 = b.x - a.x
		const dx1 = -(b.y - a.y)

		const x2 = (c.x + b.x) / 2
		const y2 = (c.y + b.y) / 2
		const dy2 = c.x - b.x
		const dx2 = -(c.y - b.y)

		const cx = (((y1 * dx1 * dx2) + (x2 * dx1 * dy2)) - (x1 * dy1 * dx2) - (y2 * dx1 * dx2)) / ((dx1 * dy2) - (dy1 * dx2));
		const cy = (((cx - x1) * dy1) / dx1) + y1;
		center_ = new Vector2(cx, cy)

		const dx = cx - a.x;
		const dy = cy - a.y;
		radius = (dx * dx) + (dy * dy)

		return { centerFC: center_, radiusFC: radius}
	}

	private static AngleValue(x1: number, y1: number, x2: number, y2: number) {
		let t: number = 0
		const dx = x2 - x1
		const ax = Math.abs(dx)
		const dy = y2 - y1
		const ay = Math.abs(dy)

		t = (ax + ay) === 0 ? 360 / 9 : dy / (ax + ay)

		if (dx < 0) {
			t = 2 - t;
		} else if (dy < 0) {
			t = 4 + t;
		}

		return t * 90;
	}

	private static HullCull(points: Vector2[]) {
		const culling_box = this.GetMinMaxBox(points)
		return points.filter(pt => pt.x <= culling_box.Left || pt.x >= culling_box.Right || pt.y <= culling_box.Top || pt.y >= culling_box.Bottom)
	}

	private static GetMinMaxBox(points: Vector2[]) {
		const ul = new Vector2(0, 0), ur = ul, ll = ul, lr = ul
		const corner = this.GetMinMaxCorners(points, /** out */ ul, /** out */ ur, /** out */ ll,  /** out */lr)

		let xmin = corner.ulC.x
		let ymin = corner.ulC.y

		let xmax = corner.urC.x
		if (ymin < corner.urC.y)
			ymin = corner.urC.y

		if (xmax > corner.lrC.x)
			xmax = corner.lrC.x

		let ymax = corner.lrC.y

		if (xmin < corner.llC.x)
			xmin = corner.llC.x

		if (ymax > corner.llC.y)
			ymax = corner.llC.y

		return new RectangleX(new Vector2(xmin, ymin), new Vector2(xmax - xmin, ymax - ymin));
	}

	private static GetMinMaxCorners(points: Vector2[], ul: Vector2, ur: Vector2, ll: Vector2, lr: Vector2) {
		ul = points[0]
		ur = ul
		ll = ul
		lr = ul

		points.forEach(pt => {

			if ((-pt.x - pt.y) > (-ul.x - ul.y))
				ul = pt

			if ((pt.x - pt.y) > (ur.x - ur.y))
				ur = pt

			if ((-pt.x + pt.y) > (-ll.x + ll.y))
				ll = pt

			if ((pt.x + pt.y) > (lr.x + lr.y))
				lr = pt

		})

		return { ulC: ul, urC: ur, llC: ll, lrC: lr }
	}
}
