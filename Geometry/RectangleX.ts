import { Vector2 } from "wrapper/Imports"

/**
 * rewrite method
 *  RectangleX,
 * 	RectangleXVector,
 * 	RectangleXVSF,
 *  RectangleSize,
 * 	MultiplySizeF.. and etc...
 */
export class RectangleX {

	constructor(public Position: Vector2 = new Vector2(0, 0), public Size: Vector2 = new Vector2(0, 0)) { }

	public get Right() {
		return this.Position.x + this.Size.x
	}

	public set Right(value: number) {
		this.Position.x = value
	}

	public get Left() {
		return this.Position.x
	}

	public set Left(value: number) {
		this.Position.x = value
	}

	public get Width() {
		return this.Size.x
	}

	public set Width(value: number) {
		this.Size.x = value
	}

	public get Height() {
		return this.Size.y
	}
	public set Height(value: number) {
		this.Size.y = value
	}

	public get Center(): Vector2 {
		return new Vector2(
			this.Position.x + this.Size.x / 2,
			this.Position.y + this.Size.y / 2,
		)
	}

	public get Top() {
		return this.Position.x + this.Size.x
	}

	public get X() {
		return this.Position.x
	}
	public set X(value: number) {
		this.Position.x = value
	}

	public get Y() {
		return this.Position.y
	}

	public set Y(value: number) {
		this.Position.y = value
	}

	public get Bottom(): number {
		return this.Position.y + this.Size.y
	}

	public get TopLeft(): Vector2 {
		return new Vector2(this.Left, this.Top)
	}

	public get TopRight(): Vector2 {
		return new Vector2(this.Right, this.Top)
	}

	public get BottomLeft(): Vector2 {
		return new Vector2(this.Left, this.Bottom)
	}

	public get BottomRight(): Vector2 {
		return new Vector2(this.Right, this.Bottom)
	}

	public Clone() {
		return new RectangleX(new Vector2(this.X, this.Y), new Vector2(this.Width, this.Height))
	}

	public MoveTopBorder(offset: number): RectangleX {
		return this.RectangleX(
			this.Position.x, this.Position.y - offset,
			this.Size.x,
			this.Size.y + offset,
		)
	}

	public MoveBottomBorder(offset: number): RectangleX {
		return this.RectangleX(this.Position.x, this.Position.y, this.Size.x, this.Size.y + offset)
	}

	public MoveRightBorder(offset: number): RectangleX {
		return this.RectangleX(this.Position.x, this.Position.y, this.Size.x + offset, this.Size.y)
	}

	public MoveLeftBorder(offset: number): RectangleX {
		return this.RectangleX(this.Position.x + offset, this.Position.y, this.Size.x - offset, this.Size.y)
	}

	public SinkToBottomRight(width: number, height: number): RectangleX {
		return this.SinkToBottomRightSize(new Vector2(width, height))
	}

	public SinkToBottomRightSize(size: Vector2): RectangleX {
		return this.RectangleSize(this.Right - size.x, this.Bottom - size.y, size)
	}

	public SinkToBottomRightVector(size: Vector2): RectangleX {
		return this.SinkToBottomRightSize(new Vector2(size.x, size.y))
	}

	public SinkToBottomLeft(width: number, height: number): RectangleX {
		return this.SinkToBottomLeftSize(new Vector2(width, height))
	}

	public SinkToBottomLeftSize(size: Vector2): RectangleX {
		return this.RectangleSize(this.X, this.Bottom - size.y, size)
	}

	public SinkToBottomLeftVector(size: Vector2): RectangleX {
		return this.SinkToBottomLeftSize(new Vector2(size.x, size.y))
	}

	public RectangleXVSF(Position: Vector2, size: Vector2) {
		this.Position = Position
		this.Size = size
		return this
	}

	public RectangleXVSV(Position: Vector2, size: Vector2) {
		this.Position = Position
		this.Size = new Vector2(size.x, size.y)
		return this
	}

	public RectangleSize(x: number, y: number, size: Vector2) {
		this.Position = new Vector2(x, y)
		this.Size = size
		return this
	}

	public RectangleXXYSV(x: number, y: number, size: Vector2) {
		this.Position = new Vector2(x, y)
		this.Size = new Vector2(size.x, size.y)
		return this
	}

	public RectangleXVector(Position: Vector2, width: number, height: number) {
		this.Position = Position
		this.Size = new Vector2(width, height)
		return this
	}

	public RectangleX(x: number, y: number, width: number, height: number): RectangleX {
		this.Position = new Vector2(x, y)
		this.Size = new Vector2(width, height)
		return this
	}

	public Contains(position: Vector2) {
		return position.x >= this.Left && position.x <= this.Right && position.y >= this.Top && position.y <= this.Bottom
	}

	public AddSizeF(size: Vector2) {
		return this.RectangleX(this.Position.x, this.Position.y, this.Size.x + size.x, this.Size.y + size.y)
	}

	public MultiplySizeF(size: Vector2) {
		const num = (this.Size.x * size.x)
		const num2 = (this.Size.y * size.y)
		return this.RectangleX(this.Position.x + (this.Width - num) / 2, this.Position.y + (this.Height - num2) / 2, num, num2)
	}

	public AddPosition(Position: Vector2) {
		this.Position.AddForThis(Position)
		return this
	}

	public MultiplyVector(Position: Vector2) {
		return this.RectangleSize(this.Position.x * Position.x, this.Position.y * Position.y, this.Size)
	}

	public AddSizeVector(size: number) {
		return this.RectangleX(this.Position.x - size, this.Position.y - size, this.Size.x + size * 2, this.Size.y + size * 2)
	}

	public SubtractSizeVector(size: number) {
		return new RectangleX(
			this.Position.AddScalar(size / 2),
			this.Size.SubtractScalar(size),
		)
	}

	public MultiplySizeFX(size: number) {
		return this.MultiplySizeF(new Vector2(size, size))
	}

	public RectangleXAdd(rec2: RectangleX) {
		const num = Math.min(this.Left, rec2.Left)
		const num2 = Math.max(this.Right, rec2.Right)
		const num3 = Math.min(this.Top, rec2.Top)
		const num4 = Math.max(this.Bottom, rec2.Bottom)
		return this.RectangleX(num, num3, num2 - num, num4 - num3)
	}

	public IsZero(tolerance: number = 0.01): boolean {
		return this.Position.IsZero(tolerance) && this.Size.IsZero(tolerance)
	}

	public toString(): string {
		return `RectangleX(${this.X},${this.Y},${this.Width},${this.Height})`
	}
}
