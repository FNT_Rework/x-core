import { Vector2 } from "wrapper/Imports";

export class GeometryX {
	public static VectorMovementCollision(startPoint1: Vector2, endPoint1: Vector2, v1: number, startPoint2: Vector2, v2: number, delay: number = 0) {
		const sP1x = startPoint1.x, sP1y = startPoint1.y, eP1x = endPoint1.x, eP1y = endPoint1.y, sP2x = startPoint2.x, sP2y = startPoint2.y
		const d = eP1x - sP1x, e = eP1y - sP1y;
		let t1 = Number.NaN
		const dist = Math.sqrt((d * d) + (e * e))
		const S = Math.abs(dist) > Number.EPSILON ? (v1 * d) / dist : 0, K = Math.abs(dist) > Number.EPSILON ? (v1 * e) / dist : 0;
		const r = sP2x - sP1x, j = sP2y - sP1y;
		const c = (r * r) + (j * j);
		if (dist > 0) {
			if (Math.abs(v1 - Number.MAX_VALUE) < Number.EPSILON) {
				const t = dist / v1;
				t1 = (v2 * t) >= 0 ? t : Number.NaN;
			} else if (Math.abs(v2 - Number.MAX_VALUE) < Number.EPSILON) {
				t1 = 0;
			} else {
				const a = ((S * S) + (K * K)) - (v2 * v2), b = (-r * S) - (j * K)

				if (Math.abs(a) < Number.EPSILON) {
					if (Math.abs(b) < Number.EPSILON) {
						t1 = Math.abs(c) < Number.EPSILON ? 0 : Number.NaN
					} else {
						const t = -c / (2 * b)
						t1 = (v2 * t) >= 0 ? t : Number.NaN
					}
				} else {
					const sqr = (b * b) - (a * c)
					if (sqr >= 0) {
						const nom = Math.sqrt(sqr)
						let t = (-nom - b) / a
						t1 = (v2 * t) >= 0 ? t : Number.NaN
						t = (nom - b) / a
						const t2 = (v2 * t) >= 0 ? t : Number.NaN

						if (!Number.isNaN(t2) && !Number.isNaN(t1)) {
							if (t1 >= delay && t2 >= delay) {
								t1 = Math.min(t1, t2);
							} else if (t2 >= delay) {
								t1 = t2
							}
						}
					}
				}
			}
		} else if (Math.abs(dist) < Number.EPSILON) {
			t1 = 0
		}

		return [t1, !Number.isNaN(t1) ? new Vector2(sP1x + (S * t1), sP1y + (K * t1)) : new Vector2()]
	}
}
