import { Color, ParticlesSDK, Unit, Vector2, Vector3 } from "wrapper/Imports";

export class PolygonX {
	public Points: Vector3[] = []
	constructor(...points: Vector3[]) {
		this.Points = points
	}

	public Add(polygon: PolygonX | Vector3): void {
		if (polygon instanceof PolygonX)
			polygon.Points.forEach(point => this.AddPoint(point))
		else
			this.AddPoint(polygon)
	}

	public Draw(unit: Unit, ParticleManager: ParticlesSDK, color: Color, width = 10, mode2D = 10) {
		for (let i = 0; i <= this.Points.length - 1; i++) {
			const nextIndex = (this.Points.length - 1 === i) ? 0 : (i + 1)
			const pos1 = new Vector3(this.Points[i].x, this.Points[i].y, 0)
			const pos2 = new Vector3(this.Points[nextIndex].x, this.Points[nextIndex].y, 0)
			if (pos1 === undefined || pos2 === undefined)
				return
			ParticleManager.DrawLine(pos1.Length, unit, pos2, {
				Position: pos1,
				Color: color,
				Width: width,
				Mode2D: mode2D,
			})
		}
	}

	public IsInside(point: Vector3) {
		return !this.IsOutside(point)
	}
	public IsOutside(point: Vector3) {
		return this.PointInPolygon(point) === 0
	}

	private PointInPolygon(point: Vector3) {
		let result = 0
		const cnt = this.Points.length
		if (cnt < 3)
			return 0

		let ip = this.Points[0]
		for (let i = 1; i <= cnt; i++) {
			const ipNext = (i === cnt) ? this.Points[0] : this.Points[i]
			if (ipNext.y === point.y && (ipNext.x === point.x || (ip.y === point.y && ipNext.x > point.x === ip.x < point.x)))
				return -1

			if (ip.y < point.y !== ipNext.y < point.y) {
				if (ip.x >= point.x) {
					if (ipNext.x > point.x) {
						result = 1 - result
					} else {
						const d = (ip.x - point.x) * (ipNext.y - point.y) - (ipNext.x - point.x) * (ip.y - point.y)
						if (d === 0)
							return -1
						if (d > 0 === ipNext.y > ip.y)
							result = 1 - result
					}
				} else if (ipNext.x > point.x) {
					const d2 = (ip.x - point.x) * (ipNext.y - point.y) - (ipNext.x - point.y) * (ip.y - point.y)
					if (d2 === 0)
						return -1
					if (d2 > 0 === ipNext.y > ip.y)
						result = 1 - result
				}
			}
			ip = ipNext
		}
		return result
	}

	private AddPoint(point: Vector3) {
		this.Points.push(point)
	}
}

export class PRectangle extends PolygonX {

	constructor(public start: Vector2, public end: Vector2, public width: number) {
		super(Vector3.FromVector2(start), Vector3.FromVector2(end))
		this.UpdatePolygon(0, -1)
	}

	private get Direction() {
		return this.end.Subtract(this.start)
	}

	private get Perpendicular() {
		return this.Direction.Perpendicular();
	}

	private UpdatePolygon(offset: number = 0, overrideWidth: number = -1) {
		this.Points = []

		this.Points.push(Vector3.FromVector2(this.start.AddScalar(
			((overrideWidth > 0) ? overrideWidth : (this.width + offset))
		).Multiply(this.Perpendicular).SubtractScalar(offset).Multiply(this.Direction)))

		this.Points.push(Vector3.FromVector2(this.start.SubtractScalar(
			((overrideWidth > 0) ? overrideWidth : (this.width + offset))
		).Multiply(this.Perpendicular).SubtractScalar(offset).Multiply(this.Direction)))

		this.Points.push(Vector3.FromVector2(this.end.SubtractScalar(
			((overrideWidth > 0) ? overrideWidth : (this.width + offset))
		).Multiply(this.Perpendicular).AddScalar(offset).Multiply(this.Direction)))

		this.Points.push(Vector3.FromVector2(this.end.AddScalar(
			((overrideWidth > 0) ? overrideWidth : (this.width + offset))
		).Multiply(this.Perpendicular).AddScalar(offset).Multiply(this.Direction)))
	}
}
