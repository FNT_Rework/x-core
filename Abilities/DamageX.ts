import {
	abaddon_death_coil, Ability,
	alchemist_unstable_concoction, ancient_apparition_chilling_touch,
	antimage_mana_void,
	arc_warden_flux,
	arc_warden_spark_wraith,
	axe_battle_hunger,
	axe_culling_blade,
	bane_brain_sap,
	bane_fiends_grip,
	bounty_hunter_shuriken_toss,
	bristleback_quill_spray,
	centaur_double_edge,
	centaur_hoof_stomp,
	chaos_knight_chaos_bolt,
	Creep,
	DAMAGE_TYPES,
	dark_willow_bedlam,
	dark_willow_bramble_maze,
	disruptor_thunder_strike,
	elder_titan_earth_splitter,
	elder_titan_echo_stomp,
	ember_spirit_flame_guard,
	ember_spirit_searing_chains,
	enchantress_impetus,
	enigma_malefice,
	furion_wrath_of_nature,
	gyrocopter_call_down,
	gyrocopter_homing_missile,
	gyrocopter_rocket_barrage,
	Hero,
	huskar_life_break,
	invoker_chaos_meteor,
	invoker_cold_snap,
	invoker_deafening_blast,
	invoker_emp,
	invoker_exort,
	invoker_ice_wall,
	invoker_quas,
	invoker_sun_strike,
	invoker_tornado,
	invoker_wex,
	item_nether_shawl,
	item_timeless_relic,
	item_trident,
	jakiro_dual_breath,
	jakiro_liquid_fire,
	juggernaut_blade_fury,
	kunkka_tidebringer,
	kunkka_torrent,
	legion_commander_duel,
	legion_commander_overwhelming_odds,
	lina_laguna_blade,
	lion_finger_of_death,
	medusa_mystic_snake,
	meepo_poof,
	necrolyte_reapers_scythe,
	nevermore_shadowraze1,
	nevermore_shadowraze2,
	nevermore_shadowraze3,
	nyx_assassin_impale,
	nyx_assassin_mana_burn,
	nyx_assassin_vendetta,
	obsidian_destroyer_sanity_eclipse,
	ogre_magi_ignite,
	omniknight_purification,
	phoenix_icarus_dive,
	phoenix_sun_ray,
	phoenix_supernova,
	puck_dream_coil,
	pudge_dismember,
	pudge_rot,
	pugna_nether_blast,
	queenofpain_shadow_strike,
	razor_plasma_field,
	rubick_arcane_supremacy,
	sandking_epicenter,
	sandking_sand_storm,
	shadow_demon_shadow_poison,
	shredder_chakram,
	shredder_chakram_2,
	shredder_whirling_death,
	silencer_curse_of_the_silent,
	silencer_last_word,
	skywrath_mage_ancient_seal,
	slark_dark_pact,
	snapfire_firesnap_cookie,
	snapfire_spit_creep,
	storm_spirit_overload,
	storm_spirit_static_remnant,
	tidehunter_anchor_smash,
	tidehunter_gush,
	tinker_laser,
	tiny_avalanche,
	tiny_toss,
	treant_leech_seed,
	treant_natures_grasp,
	treant_overgrowth,
	troll_warlord_whirling_axes_ranged,
	tusk_ice_shards,
	tusk_snowball,
	tusk_tag_team,
	undying_decay,
	undying_soul_rip,
	Unit,
	vengefulspirit_magic_missile,
	venomancer_venomous_gale,
	viper_poison_attack,
	viper_viper_strike,
	visage_gravekeepers_cloak,
	visage_soul_assumption,
	void_spirit_aether_remnant,
	void_spirit_astral_step,
	windrunner_powershot,
	witch_doctor_maledict,
	zuus_static_field,
	// enigma_midnight_pulse, => TODO
	// terrorblade_sunder, => TODO
	// morphling_adaptive_strike_agi => TODO
} from "wrapper/Imports"

export class DamageX {

	constructor(private ability: Ability, private target: Unit, private Heroes: Hero[], private Creeps: Creep[]) {
		if (this.ability === undefined) {
			console.error("XCore->DamageX->ability undefined")
			return
		}

		if (this.ability.Owner === undefined) {
			console.error("XCore->DamageX->ability Owner undefined")
			return
		}

		if (this.ability.Owner === undefined) {
			console.error("XCore->DamageX->ability Target undefined")
			return
		}
	}

	public get GetDamage(): number {
		const caster = this.ability.Owner! as Hero
		if (caster === undefined)
			return 0

		const DamageType = this.AbilityDamage[1]
		const AbilityDamage = this.AbilityDamage[0]
		if (AbilityDamage <= 0)
			return 0
		return Math.floor(Math.max(this.CalculateDamage(AbilityDamage, caster, DamageType, this.target), 0))
	}

	private get AbilityDamage() {

		let DamageType = this.ability.DamageType
		let AbilityDamage = this.ability.AbilityDamage

		const caster = this.ability.Owner!
		const WexDamage = caster.GetAbilityByClass(invoker_wex)
		const QwasDamage = caster.GetAbilityByClass(invoker_quas)
		const ExortDamage = caster.GetAbilityByClass(invoker_exort)

		if (this.ability instanceof silencer_curse_of_the_silent)
			AbilityDamage *= this.ability.GetSpecialValue("duration") - 2

		if (this.ability instanceof silencer_last_word)
			AbilityDamage += (((caster.TotalIntellect - this.target.TotalIntellect) * this.ability.GetSpecialValue("int_multiplier")))

		if (this.ability instanceof phoenix_icarus_dive)
			AbilityDamage = this.ability.GetSpecialValue("damage_per_second") * this.ability.GetSpecialValue("dive_duration")

		// var t0 = hrtime()
		if (this.ability instanceof storm_spirit_static_remnant)
			AbilityDamage = this.ability.GetSpecialValue("static_remnant_damage")
		// var t1 = hrtime()
		// console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.")

		if (this.ability instanceof phoenix_sun_ray)
			AbilityDamage = this.ability.GetSpecialValue("base_damage")

		if (this.ability instanceof phoenix_supernova)
			AbilityDamage = this.ability.GetSpecialValue("damage_per_sec") * this.ability.MaxDuration

		if (this.ability instanceof pudge_rot)
			AbilityDamage = this.ability.GetSpecialValue("rot_damage")

		if (this.ability instanceof sandking_sand_storm)
			AbilityDamage = this.ability.GetSpecialValue("sand_storm_damage")

		if (this.ability instanceof sandking_epicenter)
			AbilityDamage = this.ability.GetSpecialValue("epicenter_damage")

		if (this.ability instanceof tusk_ice_shards)
			AbilityDamage = this.ability.GetSpecialValue("shard_damage")

		if (this.ability instanceof tusk_snowball)
			AbilityDamage = this.ability.GetSpecialValue("snowball_damage")

		if (this.ability instanceof tusk_tag_team)
			AbilityDamage = this.ability.GetSpecialValue("bonus_damage")

		if (this.ability instanceof tidehunter_gush)
			AbilityDamage = this.ability.GetSpecialValue("gush_damage")

		if (this.ability instanceof tidehunter_anchor_smash)
			AbilityDamage = this.ability.GetSpecialValue("attack_damage")

		if (this.ability instanceof shredder_chakram || this.ability instanceof shredder_chakram_2)
			AbilityDamage = this.ability.GetSpecialValue("pass_damage")

		if (this.ability instanceof tiny_avalanche)
			AbilityDamage = this.ability.GetSpecialValue("avalanche_damage") * this.ability.GetSpecialValue("total_duration")

		if (this.ability instanceof tiny_toss)
			AbilityDamage = this.ability.GetSpecialValue("toss_damage")

		if (this.ability instanceof treant_natures_grasp)
			AbilityDamage = this.ability.GetSpecialValue("damage_per_second") //* this.ability.GetSpecialValue("vines_duration")

		if (this.ability instanceof treant_overgrowth)
			AbilityDamage = this.ability.GetSpecialValue("damage") * this.ability.GetSpecialValue("duration")

		// TODO tree bonus damage  (this.ability.GetSpecialValue("tree_damage_scale"))
		if (this.ability instanceof shredder_whirling_death)
			AbilityDamage = this.ability.GetSpecialValue("whirling_damage")

		if (this.ability instanceof tinker_laser)
			AbilityDamage = this.ability.GetSpecialValue("laser_damage")

		if (this.ability instanceof treant_leech_seed)
			AbilityDamage = this.ability.GetSpecialValue("leech_damage") * this.ability.GetSpecialValue("duration")

		if (this.ability instanceof snapfire_firesnap_cookie || this.ability instanceof snapfire_spit_creep)
			AbilityDamage = this.ability.GetSpecialValue("impact_damage")

		if (this.ability.Name === "magnataur_skewer")
			AbilityDamage = this.ability.GetSpecialValue("skewer_damage")

		if (this.ability.Name === "magnataur_reverse_polarity")
			AbilityDamage = this.ability.GetSpecialValue("polarity_damage")

		if (this.ability.Name === "magnataur_shockwave")
			AbilityDamage = this.ability.GetSpecialValue("shock_damage")

		if (this.ability instanceof centaur_hoof_stomp)
			AbilityDamage = this.ability.GetSpecialValue("stomp_damage")

		if (this.ability instanceof omniknight_purification)
			AbilityDamage = this.ability.GetSpecialValue("heal")

		if (this.ability instanceof kunkka_tidebringer)
			AbilityDamage = this.ability.GetSpecialValue("damage_bonus")

		if (this.ability instanceof kunkka_torrent)
			AbilityDamage = this.ability.GetSpecialValue("torrent_damage")

		if (this.ability instanceof bounty_hunter_shuriken_toss)
			AbilityDamage = this.ability.GetSpecialValue("bonus_damage")

		if (this.ability instanceof legion_commander_duel)
			AbilityDamage += caster.AttackDamage(this.target) * 2 - 1

		if (this.ability instanceof huskar_life_break)
			AbilityDamage = (this.target.HP * this.ability.GetSpecialValue("health_damage"))

		if (this.ability instanceof elder_titan_echo_stomp)
			AbilityDamage = this.ability.GetSpecialValue("stomp_damage")

		if (this.ability instanceof elder_titan_earth_splitter)
			AbilityDamage = this.target.MaxHP * (this.ability.GetSpecialValue("damage_pct") / 100) * 0.5

		if (this.ability instanceof alchemist_unstable_concoction)
			AbilityDamage = this.ability.GetSpecialValue("max_damage")

		if (this.ability instanceof bristleback_quill_spray)
			AbilityDamage = this.ability.GetSpecialValue("quill_base_damage")

		if (this.ability instanceof antimage_mana_void)
			AbilityDamage = this.ability.GetSpecialValue("mana_void_damage_per_mana") * (this.target.MaxMana - this.target.Mana) * 1

		// TODO-> % dmg, per unit
		if (this.ability instanceof furion_wrath_of_nature)
			AbilityDamage = this.ability.GetSpecialValue("damage" + (caster.HasScepter ? "_scepter" : ""))

		if (this.ability instanceof pudge_dismember)
			AbilityDamage = this.ability.GetSpecialValue("dismember_damage")

		if (this.ability instanceof windrunner_powershot)
			AbilityDamage = this.ability.GetSpecialValue("powershot_damage")

		if (this.ability instanceof undying_decay)
			AbilityDamage = this.ability.GetSpecialValue("decay_damage")

		if (this.ability instanceof ember_spirit_searing_chains)
			AbilityDamage = this.ability.GetSpecialValue("total_damage") * (this.ability.GetSpecialValue("duration"))

		if (this.ability instanceof ember_spirit_flame_guard)
			AbilityDamage = this.ability.GetSpecialValue("damage_per_second") * (this.ability.GetSpecialValue("duration"))

		if (this.ability instanceof gyrocopter_call_down)
			AbilityDamage = this.ability.GetSpecialValue("damage_first")

		if (this.ability instanceof juggernaut_blade_fury)
			AbilityDamage = this.ability.GetSpecialValue("blade_fury_damage") * (this.ability.GetSpecialValue("duration"))

		if (this.ability instanceof medusa_mystic_snake)
			AbilityDamage = this.ability.GetSpecialValue("snake_damage")

		if (this.ability instanceof nyx_assassin_impale)
			AbilityDamage = this.ability.GetSpecialValue("impale_damage")

		if (this.ability instanceof nyx_assassin_mana_burn)
			AbilityDamage = Math.min(this.ability.GetSpecialValue("float_multiplier") * this.target.TotalIntellect, this.target.Mana)

		if (this.ability instanceof nyx_assassin_vendetta)
			AbilityDamage = this.ability.GetSpecialValue("bonus_damage")

		if (this.ability instanceof gyrocopter_rocket_barrage)
			AbilityDamage = this.ability.GetSpecialValue("rocket_damage")

		if (this.ability instanceof arc_warden_spark_wraith)
			AbilityDamage = this.ability.GetSpecialValue("spark_damage")

		if (this.ability instanceof abaddon_death_coil)
			AbilityDamage = this.ability.GetSpecialValue("target_damage")

		if (this.ability instanceof meepo_poof)
			AbilityDamage = this.ability.GetSpecialValue("poof_damage")

		if (this.ability instanceof slark_dark_pact)
			AbilityDamage = this.ability.GetSpecialValue("total_damage")

		if (this.ability instanceof troll_warlord_whirling_axes_ranged)
			AbilityDamage = this.ability.GetSpecialValue("axe_damage")

		if (this.ability instanceof vengefulspirit_magic_missile)
			AbilityDamage = this.ability.GetSpecialValue("magic_missile_damage")

		if (this.ability instanceof viper_viper_strike)
			AbilityDamage += (AbilityDamage * this.ability.GetSpecialValue("duration"))

		if (this.ability instanceof bane_brain_sap)
			AbilityDamage = this.ability.GetSpecialValue("brain_sap_damage")

		if (this.ability instanceof bane_fiends_grip)
			AbilityDamage = (this.ability.GetSpecialValue("fiend_grip_damage") * this.ability.MaxChannelTime)

		if (this.ability instanceof dark_willow_bramble_maze)
			AbilityDamage = this.ability.GetSpecialValue("latch_damage")

		if (this.ability instanceof dark_willow_bedlam)
			AbilityDamage = this.ability.GetSpecialValue("attack_damage")

		if (this.ability instanceof disruptor_thunder_strike)
			AbilityDamage = (this.ability.GetSpecialValue("strike_damage") * this.ability.GetSpecialValue("strikes"))

		if (this.ability instanceof enigma_malefice)
			AbilityDamage = (this.ability.GetSpecialValue("damage") * this.ability.GetSpecialValue("tooltip_stuns"))

		if (this.ability instanceof jakiro_dual_breath)
			AbilityDamage = (this.ability.GetSpecialValue("burn_damage") * this.ability.MaxDuration)

		if (this.ability instanceof ogre_magi_ignite)
			AbilityDamage = (this.ability.GetSpecialValue("burn_damage") * this.ability.GetSpecialValue("duration"))

		if (this.ability instanceof puck_dream_coil)
			AbilityDamage = this.ability.GetSpecialValue("coil_initial_damage")

		if (this.ability instanceof void_spirit_aether_remnant)
			AbilityDamage = this.ability.GetSpecialValue("impact_damage")

		if (this.ability instanceof pugna_nether_blast)
			AbilityDamage = this.ability.GetSpecialValue("blast_damage")

		if (this.ability instanceof jakiro_liquid_fire)
			AbilityDamage *= this.ability.MaxDuration

		if (this.ability instanceof witch_doctor_maledict)
			AbilityDamage *= this.ability.MaxDuration

		if (this.ability instanceof void_spirit_astral_step) {
			DamageType = DAMAGE_TYPES.DAMAGE_TYPE_MAGICAL
			AbilityDamage = this.ability.GetSpecialValue("pop_damage")
		}

		if (this.ability instanceof visage_soul_assumption) {
			const BaseDamageData = this.ability.GetSpecialValue("soul_base_damage")
			const BonusDamageData = this.ability.GetSpecialValue("soul_charge_damage")
			const modifier = caster.Buffs.find(x => x.Name === "modifier_visage_soul_assumption")
			const num = (modifier !== undefined) ? modifier.StackCount : 0
			AbilityDamage = BaseDamageData + (BonusDamageData * num)
		}

		if (this.ability instanceof queenofpain_shadow_strike) {
			const DamageBase = this.ability.GetSpecialValue("strike_damage")
			const DurationDamage = this.ability.GetSpecialValue("duration_damage")
			AbilityDamage = DamageBase + ((DurationDamage * this.ability.MaxDuration) / this.ability.GetSpecialValue("damage_interval"))
		}

		if (this.ability instanceof obsidian_destroyer_sanity_eclipse) {
			const DamageBase = this.ability.GetSpecialValue("base_damage")
			const DamageMulty = this.ability.GetSpecialValue("damage_multiplier")
			const num = Math.max(caster.MaxMana - this.target.MaxMana, 0)
			AbilityDamage = DamageBase + (num * DamageMulty)
		}

		if (this.ability instanceof shadow_demon_shadow_poison) {
			const StackDamage = this.ability.GetSpecialValue("stack_damage")
			const modifier = this.target.Buffs.find(x => x.Name === "modifier_shadow_demon_shadow_poison")
			const BonusStackDamage = this.ability.GetSpecialValue("bonus_stack_damage")
			AbilityDamage = (StackDamage * (2 ** (Math.min(modifier?.StackCount ?? 0, 5) - 1))) + (Math.min(modifier?.StackCount ?? 0, 5 - 5)) * BonusStackDamage
		}

		// TODO to improve code
		if (QwasDamage !== undefined && this.ability instanceof invoker_cold_snap) {
			const level = !caster.HasScepter ? QwasDamage.Level : QwasDamage.Level + 1
			AbilityDamage = this.ability.GetSpecialValue("freeze_damage", level)
		}

		// TODO to improve code
		if (WexDamage !== undefined && this.ability instanceof invoker_emp) {
			const level = !caster.HasScepter ? WexDamage.Level : WexDamage.Level + 1
			const DamageData = this.ability.GetSpecialValue("mana_burned", level)
			const DamagePctData = (this.ability.GetSpecialValue("damage_per_mana_pct") / 100)
			AbilityDamage = (Math.min(this.target.Mana, DamageData) * DamagePctData)
		}

		// TODO to improve code
		if (ExortDamage !== undefined && this.ability instanceof invoker_chaos_meteor) {
			const level = !caster.HasScepter ? ExortDamage.Level : ExortDamage.Level + 1
			AbilityDamage = this.ability.GetSpecialValue("main_damage", level)
		}

		// TODO to improve code
		if (WexDamage !== undefined && this.ability instanceof invoker_tornado) {
			const level = !caster.HasScepter ? WexDamage.Level : WexDamage.Level + 1
			AbilityDamage = this.ability.GetSpecialValue("base_damage") + this.ability.GetSpecialValue("wex_damage", level)
		}

		// TODO to improve code & added damage / count units
		if (ExortDamage !== undefined && this.ability instanceof invoker_sun_strike) {
			const level = !caster.HasScepter ? ExortDamage.Level : ExortDamage.Level + 1
			AbilityDamage = this.ability.GetSpecialValue("damage", level)
		}

		// TODO to improve code
		if (ExortDamage !== undefined && this.ability instanceof invoker_ice_wall) {
			const level = !caster.HasScepter ? ExortDamage.Level : ExortDamage.Level + 1
			AbilityDamage = this.ability.GetSpecialValue("damage_per_second", level)
		}

		// TODO to improve code
		if (ExortDamage !== undefined && this.ability instanceof invoker_deafening_blast) {
			const level = !caster.HasScepter ? ExortDamage.Level : ExortDamage.Level + 1
			AbilityDamage = this.ability.GetSpecialValue("damage", level)
		}

		// TODO to improve code
		if (this.ability instanceof enchantress_impetus) {
			const DamageData = this.ability.GetSpecialValue("distance_damage_pct")
			const DistanceCap = this.ability.GetSpecialValue("distance_cap")
			AbilityDamage = (DamageData / 100 * Math.min(caster.Distance(this.target), DistanceCap)) + (caster.AttackDamage(this.target, true) / 2)
		}

		if (this.ability instanceof ancient_apparition_chilling_touch) {
			DamageType = DAMAGE_TYPES.DAMAGE_TYPE_MAGICAL
			AbilityDamage = (this.ability.GetSpecialValue("damage") + caster.AttackDamage(this.target, true))
		}

		if (this.ability instanceof storm_spirit_overload) {
			const modifier = caster.HasBuffByName("modifier_storm_spirit_overload")
			AbilityDamage = modifier ? this.ability.GetSpecialValue("overload_damage") : 0
		}

		// TODO => enigma_midnight_pulse
		// if (this.ability instanceof enigma_midnight_pulse)
		// 	AbilityDamage = ((this.target.HPPercent / this.ability.GetSpecialValue("damage_percent")) * this.ability.GetSpecialValue("duration"))

		if (this.ability instanceof venomancer_venomous_gale) {
			const DamageData = this.ability.GetSpecialValue("strike_damage")
			AbilityDamage = DamageData + (this.ability.GetSpecialValue("tick_damage")
				* this.ability.GetSpecialValue("duration") / this.ability.GetSpecialValue("tick_interval")
				- this.target.HPRegen
			)
		}

		if (this.ability instanceof viper_poison_attack) {
			const modifier = this.target.Buffs.find(x => x.Name === "modifier_viper_poison_attack_slow")
			const num = (modifier !== undefined) ? modifier.StackCount : 0
			AbilityDamage += (num * this.ability.GetSpecialValue("duration")) + caster.AttackDamage(this.target)
		}

		if (this.ability instanceof razor_plasma_field) {
			const num = Math.max(this.target.Distance(caster) - 100, 0)
			const damageMin = this.ability.GetSpecialValue("damage_min")
			const damageMax = this.ability.GetSpecialValue("damage_max")
			AbilityDamage = Math.max(Math.min(num / this.ability.AOERadius * damageMax, damageMax), damageMin)
		}

		// TODO => terrorblade_sunder
		// if (this.ability instanceof terrorblade_sunder) {
		// 	DamageType = DAMAGE_TYPES.DAMAGE_TYPE_HP_REMOVAL
		// 	AbilityDamage = caster.MaxHP - (caster.HP) / this.target.HPPercent
		// }

		if (this.ability instanceof nevermore_shadowraze1 || this.ability instanceof nevermore_shadowraze2 || this.ability instanceof nevermore_shadowraze3) {
			const DamageData = this.ability.GetSpecialValue("shadowraze_damage")
			const bonusDamageData = this.ability.GetSpecialValue("stack_bonus_damage")
			const modifier = this.target.Buffs.find(x => x.Name === "modifier_nevermore_shadowraze_debuff")
			const num = (modifier !== undefined) ? modifier.StackCount : 0
			AbilityDamage = DamageData + (num * bonusDamageData)
		}

		if (this.ability instanceof gyrocopter_homing_missile) {
			if (this.ability.CurrentCharges !== 0)
				AbilityDamage *= this.ability.CurrentCharges
		}

		if (this.ability instanceof arc_warden_flux) {

			const FilterHeroes = this.Heroes.filter(x => x.IsEnemy())

			const FilterCreep = this.Creeps.filter(x => x.IsEnemy()
				&& x.IsWaitingToSpawn
				&& x.IsLaneCreep,
			)

			const num = [...FilterHeroes, ...FilterCreep].find(x => x !== this.target
				&& x.IsVisible
				&& x.IsAlive
				&& x.Distance(this.target) < this.ability.GetSpecialValue("search_radius"),
			)

			let dur = 0
			if (num === undefined)
				dur = this.ability.GetSpecialValue("duration")

			AbilityDamage = this.ability.GetSpecialValue("damage_per_second") * dur
		}

		if (this.ability instanceof undying_soul_rip) {

			const DamageData = this.ability.GetSpecialValue("damage_per_unit")
			const num = [...this.Heroes, ...this.Creeps].filter(x => x !== caster
				&& x !== this.target
				&& x.IsVisible
				&& x.IsAlive
				&& x.Distance(this.target) < this.ability.AOERadius,
			).length

			AbilityDamage = (DamageData * num)
		}

		if (this.ability instanceof necrolyte_reapers_scythe) {
			const After = this.GetHealthAfter(this.target, this.ability.CastPoint + (1.3 /** backswing */))
			const DamagePerMissHP = this.ability.GetSpecialValue("damage_per_health")
			AbilityDamage = ((this.target.MaxHP - After) * DamagePerMissHP)
		}

		if (this.ability instanceof lina_laguna_blade) {
			if (caster.HasScepter)
				DamageType = DAMAGE_TYPES.DAMAGE_TYPE_PURE
		}

		if (this.ability instanceof legion_commander_overwhelming_odds) {
			const DamageData = this.ability.AbilityDamage
			const RadiusData = this.ability.AOERadius
			const heroDamageData = this.ability.GetSpecialValue("damage_per_hero")
			const unitDamageData = this.ability.GetSpecialValue("damage_per_unit")
			const creeps = this.Creeps.filter(x => x.IsAlive
				&& !x.IsInvulnerable
				&& !x.IsMagicImmune
				&& x.IsVisible
				&& x.IsEnemy()
				&& x.Distance(this.target) < RadiusData).length
			const heroes = this.Heroes.filter(x => x.IsAlive
				&& x !== caster
				&& !x.IsInvulnerable
				&& !x.IsMagicImmune
				&& x.IsVisible
				&& x.IsEnemy()
				&& x.Distance(this.target) < RadiusData).length
			AbilityDamage = DamageData + (heroDamageData * heroes) + (unitDamageData * creeps)
		}

		if (this.ability instanceof centaur_double_edge) {
			const DamageData = this.ability.GetSpecialValue("edge_damage")
			const StrDamage = this.ability.GetSpecialValue("strength_damage")
			AbilityDamage = DamageData + Math.ceil(caster.TotalStrength) * StrDamage / 100
		}

		if (this.ability instanceof chaos_knight_chaos_bolt) {
			const damage_min = this.ability.GetSpecialValue("damage_min")
			const damage_max = this.ability.GetSpecialValue("damage_max")
			AbilityDamage = (damage_min + damage_max) / 2
		}

		// TODO instanceof
		// if (this.ability.Name === "hoodwink_acorn_shot") {
		// 	DamageType = DAMAGE_TYPES.DAMAGE_TYPE_PHYSICAL
		// 	AbilityDamage = this.ability.GetSpecialValue("bonus_damage")
		// }

		// TODO instanceof
		if (this.ability.Name === "hoodwink_sharpshooter")
			AbilityDamage = this.ability.GetSpecialValue("max_damage")

		// TODO instanceof
		if (this.ability.Name === "hoodwink_bushwhack")
			AbilityDamage = this.ability.GetSpecialValue("total_damage")

		// TODO => morphling_adaptive_strike_agi
		// if (this.ability instanceof morphling_adaptive_strike_agi) {
		// 	const totalAgility = caster.TotalAgility
		// 	const totalStrength = caster.TotalStrength
		// 	const damage_min = this.ability.GetSpecialValue("damage_min")
		// 	const damage_max = this.ability.GetSpecialValue("damage_max")
		// 	let num = (totalAgility * 0.75 > totalStrength) ? damage_max : damage_min
		// 	let num2 = totalAgility * num
		// 	AbilityDamage = num2
		// 	console.log(AbilityDamage, "totalAgility", totalAgility, "totalStrength", totalStrength)
		// }

		if (this.ability instanceof lion_finger_of_death) {
			let damage = this.ability.GetSpecialValue("damage" + (caster.HasScepter ? "_scepter" : ""))
			const buff = this.target.GetBuffByName("modifier_lion_finger_of_death_kill_counter")
			if (buff !== undefined)
				damage += buff.StackCount * this.ability.GetSpecialValue("damage_per_kill")
			AbilityDamage = damage
		}

		if (this.ability instanceof axe_culling_blade) {
			const healthAfter = this.GetHealthAfter(this.target, this.ability.CastPoint)
			const killThreshold = this.ability.GetSpecialValue("kill_threshold")
			AbilityDamage = healthAfter > killThreshold ? AbilityDamage : killThreshold
		}

		if (this.ability instanceof axe_battle_hunger) {
			const damage = this.ability.GetSpecialValue("damage_per_second") * this.ability.GetSpecialValue("duration")
			AbilityDamage = damage
		}

		return [AbilityDamage, DamageType]
	}

	private GetHealthAfter(unit: Unit, delay: number) {
		if (delay <= 0 || delay === undefined)
			return unit.HP
		return Math.max(Math.min(unit.HP + unit.HPRegen * delay, unit.MaxHP), 0)
	}

	private AmplifyDamageBuffs(damage: number, damage_type: DAMAGE_TYPES, caster: Unit, target: Unit) {
		[caster, target].forEach(unit =>
			damage = unit.AbsorbedDamage(damage, damage_type, target))
		return Math.max(damage, 0)
	}

	private CalculateDamage(base_damage: number, caster: Hero, damage_type: DAMAGE_TYPES, target: Unit) {
		if (base_damage <= 0 || target.WillIgnore(damage_type))
			return 0

		base_damage = this.AmplifyDamageBuffs(base_damage, damage_type, caster, target)

		const timeless_relic = caster.GetItemByClass(item_timeless_relic)
		if (timeless_relic !== undefined)
			base_damage *= (1 + (timeless_relic.GetSpecialValue("spell_amp") / 100))

		const nether_shawl = caster.GetItemByClass(item_nether_shawl)
		if (nether_shawl !== undefined)
			base_damage *= (1 + (nether_shawl.GetSpecialValue("bonus_spell_amp") / 100))

		const trident = caster.GetItemByClass(item_trident)
		if (trident !== undefined)
			base_damage *= (1 + (trident.GetSpecialValue("spell_amp") / 100))

		const static_field = caster.GetAbilityByClass(zuus_static_field)
		if (static_field !== undefined)
			base_damage += (static_field.GetSpecialValue("damage_health_pct") / 100) * target.HP

		const arcane_supremacy = caster.GetAbilityByClass(rubick_arcane_supremacy)
		if (arcane_supremacy !== undefined)
			base_damage *= (1 + (arcane_supremacy.GetSpecialValue("spell_amp") / 100))

		const ancient_seal = caster.GetAbilityByClass(skywrath_mage_ancient_seal)
		if (ancient_seal !== undefined)
			base_damage *= (1 + (ancient_seal.GetSpecialValue("resist_debuff") / -100))

		const gravekeepers_cloak = this.target.GetAbilityByClass(visage_gravekeepers_cloak)
		if (gravekeepers_cloak !== undefined && gravekeepers_cloak.Level > 0) {
			const modifier = this.target.Buffs.find(x => x.Name === "modifier_visage_gravekeepers_cloak")
			const amplifierData = gravekeepers_cloak.GetSpecialValue("damage_reduction")
			const maxDamageReductionData = gravekeepers_cloak.GetSpecialValue("max_damage_reduction")
			const amplyReduced = Math.max((amplifierData / -100) * (modifier !== undefined ? modifier.StackCount : 0),
				(maxDamageReductionData / -100))
			base_damage *= (1 + amplyReduced)
		}

		if (base_damage <= 0)
			return 0

		switch (damage_type) {
			case DAMAGE_TYPES.DAMAGE_TYPE_MAGICAL:
				base_damage *= (1 - target.MagicDamageResist / 100) * (1 + (caster.SpellAmplification / 100))
				break
		}

		return Math.max(base_damage, 0)
	}
}
