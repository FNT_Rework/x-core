import { Ability, GameSleeper, skywrath_mage_ancient_seal } from "wrapper/Imports";

export class IAbilityData {

	/** * KEY = "ActionSleeper" */
	public ActionSleeper = new GameSleeper();

	constructor(public base: Ability) {}

	public get IsSilenced() {
		return this.base instanceof skywrath_mage_ancient_seal
	}
}
