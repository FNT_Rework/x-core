export enum CollisionTypesX {
	None = 0,
	AllyCreeps = 2,
	EnemyCreeps = 4,
	AllyHeroes = 8,
	EnemyHeroes = 16,
	Runes = 32,
	Trees = 64,
	AllUnits = 30,
	AlliedUnits = 10,
	EnemyUnits = 20,
}

export enum SkillShotTypeX {
	None,
	AreaOfEffect,
	RangedAreaOfEffect,
	Line,
	Circle,
	Cone,
}

export enum HitChanceX {
	Impossible,
	Low,
	Medium,
	High,
	Immobile,
}
