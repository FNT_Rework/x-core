import { CPlayerX } from "../Entity/CPlayerX";
import { Color, GameSleeper, Unit } from "../wrapper/Imports";

export class PlayerX {

	public static UNITS: string[] = [
		"npc_dota_pugna_nether_ward",
		"npc_dota_lone_druid_bear",
		"npc_dota_warlock_golem",
		"npc_dota_visage_familiar",
		"npc_dota_lycan_wolf",
		"npc_dota_weaver_swarm",
		"npc_dota_rattletrap_cog",
		"npc_dota_unit_tombstone",
		"npc_dota_beastmaster_boar",
		"npc_dota_beastmaster_greater_boar",
		"npc_dota_beastmaster_hawk",
		"npc_dota_greater_hawk",
		"npc_dota_scout_hawk",
		"npc_dota_phoenix_sun",
		"npc_dota_shadow_shaman_ward",
		"npc_dota_techies_stasis_trap",
		"npc_dota_techies_remote_mine",
		"npc_dota_techies_land_mine",
		"npc_dota_zeus_cloud",
		"npc_dota_venomancer_plague_ward",
		"npc_dota_broodmother_spiderite",
		"npc_dota_invoker_forged_spirit",
		"npc_dota_warlock_golem_scepter",
		"npc_dota_juggernaut_healing_ward",
		"npc_dota_necronomicon_warrior",
		"npc_dota_templar_assassin_psionic_trap",
		"npc_dota_necronomicon_archer",
		"npc_dota_warlock_golem",
		"npc_dota_broodmother_spiderling",
		"npc_dota_clinkz_skeleton_archer",
	]

	public static ExcludeUnitNameLH: string[] = [
		"npc_dota_gyrocopter_homing_missile",
		"npc_dota_techies_remote_mine",
		"npc_dota_techies_land_mine",
		"npc_dota_techies_stasis_trap",
		"npc_dota_weaver_swarm",
	]

	public static Sleeper = new GameSleeper()
	public static DataX = new Map<Unit, CPlayerX>()

	public static ColorX = new Map<number, Color>()
		.set(0, new Color(51, 117, 255))
		.set(1, new Color(102, 255, 191))
		.set(2, new Color(191, 0, 191))
		.set(3, new Color(243, 240, 11))
		.set(4, new Color(255, 107, 0))
		.set(5, new Color(254, 134, 194))
		.set(6, new Color(161, 180, 71))
		.set(7, new Color(101, 217, 247))
		.set(8, new Color(0, 131, 33))
		.set(9, new Color(164, 105, 0))

	public static PauseCooldown = 0
	public static CanBeUsePause = true

	public static Dispose() {
		this.DataX.clear()
		this.Sleeper.FullReset()
		this.PauseCooldown = 0
		this.CanBeUsePause = true
	}
}
