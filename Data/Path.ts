export class PathX {

	public static PathXCore = "gitlab.com/FNT_Rework/x-core/scripts_files"
	public static readonly RunePath = `${PathX.PathXCore}/images/runes/`
	public static readonly DOTAPathItemIcon = "panorama/images/items/"
	public static readonly DOTAPathAbilityIcon = "panorama/images/spellicons/"
	public static readonly DOTAPathHeroes = "panorama/images/heroes/"
	public static readonly DOTAPathHeroesIcon = "panorama/images/heroes/icons/"

	public static Images = {
		// panorama
		topbar_mana: "panorama/images/hud/reborn/topbar_mana_psd.vtex_c",
		topbar_health: "panorama/images/hud/reborn/topbar_health_psd.vtex_c",
		topbar_health_dire: "panorama/images/hud/reborn/topbar_health_dire_psd.vtex_c",
		topbar_health_blind: "panorama/images/hud/reborn/topbar_health_colorblind_psd.vtex_c",
		buyback_header: "panorama/images/hud/reborn/buyback_header_psd.vtex_c",
		buyback_topbar: "panorama/images/hud/reborn/topbar_buyback_psd.vtex_c",
		buyback_topbar_alive: "panorama/images/hud/reborn/buyback_topbar_alive_psd.vtex_c",
		ult_cooldown: "panorama/images/hud/reborn/ult_cooldown_psd.vtex_c",
		buff_outline: "panorama/images/hud/reborn/buff_outline_psd.vtex_c",
		softedge_circle_sharp: "panorama/images/masks/softedge_circle_sharp_png.vtex_c",
		ult_no_mana: "panorama/images/hud/reborn/ult_no_mana_psd.vtex_c",
		ult_ready: "panorama/images/hud/reborn/ult_ready_psd.vtex_c",
		ult_ready_blind: `${PathX.PathXCore}/images/panels/ult_ready_blind_psd.png`,
		chat_preview_opacity_mask: "panorama/images/masks/chat_preview_opacity_mask_png.vtex_c",
		arrow_gold_dif: "panorama/images/hud/reborn/arrow_gold_dif_psd.vtex_c",
		arrow_gold_dif_blind: `${PathX.PathXCore}/images/panels/arrow_gold_dif_blind_psd.png`,
		arrow_plus_stats_red: "panorama/images/hud/reborn/arrow_plus_stats_red_psd.vtex_c",
		softedge_horizontal: "panorama/images/masks/softedge_horizontal_png.vtex_c",
		levelup_button_5: "panorama/images/hud/reborn/levelup_button_5_psd.vtex_c",
		icon_damage: "panorama/images/hud/reborn/icon_damage_psd.vtex_c",
		icon_speed: "panorama/images/hud/reborn/icon_speed_psd.vtex_c",
		icon_dota_plus: "panorama/images/plus/achievements/plus_icon_png.vtex_c",
		icon_send_message: "panorama/images/profile/icon_send_message_psd.vtex_c",
		icon_scan: "panorama/images/hud/reborn/icon_scan_on_psd.vtex_c",
		icon_ward: "panorama/images/icon_ward_psd.vtex_c",
		icon_ward_observer: "panorama/images/items/ward_observer_png.vtex_c",
		icon_ward_sentry: "panorama/images/items/ward_sentry_png.vtex_c",
		icon_settings: "panorama/images/control_icons/settings_png.vtex_c",
		check_png: "panorama/images/control_icons/check_png.vtex_c",
		icon_roshan: "panorama/images/hud/icon_roshan_psd.vtex_c",
		icon_glyph_small: "panorama/images/hud/icon_glyph_small_psd.vtex_c",
		kill_mask: "panorama/images/status_icons/modifier_kill_effect_psd.vtex_c",
		bg_deathsummary: `${PathX.PathXCore}/images/panels/item_purchase_bg_psd.png`,
		gold_large: "panorama/images/hud/reborn/gold_large_png.vtex_c",
		transfer_arrow_png: "panorama/images/hud/reborn/toast_neutral_item_transfer_arrow_png.vtex_c",
		tower_radiant: "panorama/images/heroes/npc_dota_hero_tower_radiant_png.vtex_c",
		tower_dire: "panorama/images/heroes/npc_dota_hero_tower_dire_png.vtex_c",
		outpost: "panorama/images/hud/icon_outpost_psd.vtex_c",
		roshan_timer_roshan: "panorama/images/hud/reborn/roshan_timer_roshan_psd.vtex_c",
		outpost_lose: "panorama/images/hud/icon_outpost_lost_psd.vtex_c",
		outpost_captured: "panorama/images/hud/icon_outpost_captured_psd.vtex_c",
		hardsupport: "panorama/images/rank_tier_icons/handicap/hardsupporticon_psd.vtex_c",
		softsupport: "panorama/images/rank_tier_icons/handicap/softsupporticon_psd.vtex_c",
		offlane: "panorama/images/rank_tier_icons/handicap/offlaneicon_psd.vtex_c",
		midlane: "panorama/images/rank_tier_icons/handicap/midlaneicon_psd.vtex_c",
		safelane: "panorama/images/rank_tier_icons/handicap/safelaneicon_psd.vtex_c",
		roshan_slam: "panorama/images/spellicons/roshan_slam_png.vtex_c",
		roshan_bash: "panorama/images/spellicons/roshan_bash_png.vtex_c",
		roshans_gift: "panorama/images/econ/tools/roshans_gift_png.vtex_c",
		roshan_halloween_levels: "panorama/images/spellicons/roshan_halloween_levels_png.vtex_c",
		roshan_halloween_angry: "panorama/images/spellicons/roshan_halloween_angry_png.vtex_c",
		primary_attribute_strength: "panorama/images/primary_attribute_icons/primary_attribute_icon_strength_psd.vtex_c",
		primary_attribute_agility: "panorama/images/primary_attribute_icons/primary_attribute_icon_agility_psd.vtex_c",
		primary_attribute_intelligence: "panorama/images/primary_attribute_icons/primary_attribute_icon_intelligence_psd.vtex_c",
	}

	public static readonly DOTAItems = (name: string | undefined) => {
		if (name !== undefined) {
			return `${PathX.DOTAPathItemIcon}${name && !name.includes("recipe_")
				? name.replace("item_", "")
				: "recipe"}_png.vtex_c`
		}
		return `${PathX.DOTAPathItemIcon}emptyitembg_png.vtex_c`
	}

	public static readonly Heroes = (name: string, iconSmall: boolean = true) => {
		return iconSmall
			? `${PathX.DOTAPathHeroesIcon + name}_png.vtex_c`
			: `${PathX.DOTAPathHeroes + name}_png.vtex_c`
	}

	public static Runes = (name: string, small: boolean = false) => {
		return !small ? PathX.RunePath + name + ".png"
			: PathX.RunePath + "mini/" + name + ".png"
	}

	public static readonly DOTAAbilities = (name: string) => {
		return PathX.DOTAPathAbilityIcon + name + "_png.vtex_c"
	}
}
