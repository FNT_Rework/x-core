import { Building, Courier, Creep, Hero, NeutralSpawner, Outpost, TeamData, Tower, Unit } from "wrapper/Imports";
import { CampsX } from "../Entity/Camps";

export class EntityX {

	public static AllyUnits: Unit[] = []
	public static EnemyUnits: Unit[] = []

	public static NeutralCreepX: Creep[] = []
	public static TeamDataX: Nullable<TeamData>
	public static UnitsIsControllable: Unit[] = []
	public static Camps = new Map<string, CampsX>()
	public static NeutralSpawnBoxes: NeutralSpawner[] = []

	public static AllyHeroes: Hero[] = []
	public static EnemyHeroes: Hero[] = []

	public static AllyCreeps: Creep[] = []
	public static EnemyCreeps: Creep[] = []

	public static AllyCourier: Courier[] = []
	public static EnemyCourier: Courier[] = []

	public static OutPosts: Outpost[] = []

	public static AllyTowers: Tower[] = []
	public static EnemyTowers: Tower[] = []

	public static AllyBuilding: Building[] = []
	public static EnemyBuilding: Building[] = []

	public static AllyRemoteMine: Unit[] = []
	public static EnemyRemoteMine: Unit[] = []

	public static Dispose() {
		this.Camps.clear()
		this.OutPosts = []
		this.AllyUnits = []
		this.EnemyUnits = []
		this.AllyCreeps = []
		this.AllyHeroes = []
		this.EnemyCreeps = []
		this.EnemyHeroes = []
		this.NeutralCreepX = []
		this.AllyTowers = []
		this.EnemyTowers = []
		this.AllyBuilding = []
		this.EnemyBuilding = []
		this.AllyCourier = []
		this.EnemyCourier = []
		this.AllyRemoteMine = []
		this.EnemyRemoteMine = []
		this.TeamDataX = undefined
		this.NeutralSpawnBoxes = []
		this.UnitsIsControllable = []
	}
}
