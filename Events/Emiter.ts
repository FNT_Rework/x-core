import { ArrayExtensions, Building, Courier, Creep, Events, EventsSDK, Hero, LocalPlayer, NeutralSpawner, Outpost, RendererSDK, SOType, TeamData, TechiesMines, Tower, Unit, Vector2 } from "wrapper/Imports"
import { EntityX } from "../Data/Entity"
import { PlayerX } from "../Data/PlayerX"
import { CampManagerX } from "../Entity/Camps"
import { GameX } from "../Helper/GameX"
import { EventsX } from "./EventsX"

let IsGameStarted = false
let IsGameStartedPlayer = false

let UnitsLocal: Unit[] = []
let HeroesLocal: Hero[] = []
let CreepsLocal: Creep[] = []
let TowersLocal: Tower[] = []
let CourierLocal: Courier[] = []
let OutpostLocal: Outpost[] = []
let RemoteMineLocal: Unit[] = []
let BuildingsLocal: Building[] = []

const campManager = new CampManagerX()
const ScreenSizeTemp = new Vector2(0, 0)

EventsSDK.on("Draw", async () => {
	const WindowSize = RendererSDK.WindowSize
	if (ScreenSizeTemp.Equals(WindowSize))
		return
	await EventsX.emit("WindowChange", false, WindowSize, ScreenSizeTemp)
	ScreenSizeTemp.SetVector(WindowSize.x, WindowSize.y)
})

EventsSDK.on("Draw", async () => {
	if (!GameX.IsInGame
		|| LocalPlayer === undefined
		|| LocalPlayer.Hero === undefined
		|| IsGameStartedPlayer)
		return
	await EventsX.emit("GameStarted", false, LocalPlayer.Hero)
	IsGameStartedPlayer = true
})

async function EmitIsControllable(units: Unit[], UnitsIsControllableX: Unit[]) {
	const newUnitsCtrl = units.filter(x => x.IsControllable && x.IsAlive)
	UnitsIsControllableX.forEach(async unit => {
		if (!newUnitsCtrl.includes(unit))
			await EventsX.emit("removeControllable", false, unit)
	})
	return newUnitsCtrl
}

function LocalDispose() {
	UnitsLocal = []
	TowersLocal = []
	CreepsLocal = []
	HeroesLocal = []
	CourierLocal = []
	OutpostLocal = []
	BuildingsLocal = []
	RemoteMineLocal = []
}

EventsSDK.on("PostDataUpdate", async () => {
	if (!GameX.IsInGame)
		return

	campManager.Init()
	campManager.Update()

	EntityX.AllyUnits = UnitsLocal.filter(x => !x.IsEnemy())
	EntityX.EnemyUnits = UnitsLocal.filter(x => x.IsEnemy())

	EntityX.OutPosts = OutpostLocal
	EntityX.AllyHeroes = HeroesLocal.filter(x => !x.IsEnemy())
	EntityX.EnemyHeroes = HeroesLocal.filter(x => x.IsEnemy())

	EntityX.AllyBuilding = BuildingsLocal.filter(x => !x.IsEnemy())
	EntityX.EnemyBuilding = BuildingsLocal.filter(x => x.IsEnemy())

	EntityX.AllyCreeps = CreepsLocal.filter(x => !x.IsEnemy())
	EntityX.EnemyCreeps = CreepsLocal.filter(x => x.IsEnemy())

	EntityX.AllyTowers = TowersLocal.filter(x => !x.IsEnemy())
	EntityX.EnemyTowers = TowersLocal.filter(x => x.IsEnemy())

	EntityX.AllyCourier = CourierLocal.filter(x => !x.IsEnemy())
	EntityX.EnemyCourier = CourierLocal.filter(x => x.IsEnemy())

	EntityX.AllyRemoteMine = RemoteMineLocal.filter(x => !x.IsEnemy())
	EntityX.EnemyRemoteMine = RemoteMineLocal.filter(x => x.IsEnemy())

	EntityX.UnitsIsControllable = await EmitIsControllable(EntityX.AllyHeroes, EntityX.UnitsIsControllable)
})

EventsSDK.on("GameStarted", () => {
	IsGameStarted = true
})

EventsSDK.on("GameEnded", async () => {
	await EventsX.emit("GameEnded", false)
	IsGameStarted = false
	IsGameStartedPlayer = false
})

EventsX.on("GameEnded", () => {
	LocalDispose()
	EntityX.Dispose()
	PlayerX.Dispose()
	campManager.Dispose()
})

EventsSDK.on("EntityCreated", ent => {

	if (ent instanceof Unit) {

		if (ent instanceof TechiesMines && ent.ModelName === "models/heroes/techies/fx_techies_remotebomb.vmdl")
			RemoteMineLocal.push(ent)

		if (ent instanceof Hero)
			HeroesLocal.push(ent)

		if (ent instanceof Courier)
			CourierLocal.push(ent)

		if (ent instanceof Creep)
			CreepsLocal.push(ent)

		if (ent instanceof Building) {

			if (ent instanceof Tower)
				TowersLocal.push(ent)

			BuildingsLocal.push(ent)
		}

		UnitsLocal.push(ent)
	}

	if (ent instanceof TeamData)
		EntityX.TeamDataX = ent

	if (ent instanceof NeutralSpawner)
		EntityX.NeutralSpawnBoxes.push(ent)

	if (ent instanceof Outpost)
		OutpostLocal.push(ent)
})

EventsSDK.on("EntityDestroyed", ent => {

	if (ent instanceof TeamData)
		EntityX.TeamDataX = undefined

	if (ent instanceof NeutralSpawner)
		ArrayExtensions.arrayRemove(EntityX.NeutralSpawnBoxes, ent)

	if (ent instanceof Outpost)
		ArrayExtensions.arrayRemove(OutpostLocal, ent)

	if (ent instanceof Unit) {

		if (ent instanceof TechiesMines && ent.ModelName === "models/heroes/techies/fx_techies_remotebomb.vmdl")
			ArrayExtensions.arrayRemove(RemoteMineLocal, ent)

		if (ent instanceof Hero)
			ArrayExtensions.arrayRemove(HeroesLocal, ent)

		if (ent instanceof Courier)
			ArrayExtensions.arrayRemove(CourierLocal, ent)

		if (ent instanceof Creep)
			ArrayExtensions.arrayRemove(CreepsLocal, ent)

		if (ent instanceof Building) {
			if (ent instanceof Tower)
				ArrayExtensions.arrayRemove(TowersLocal, ent)
			ArrayExtensions.arrayRemove(BuildingsLocal, ent)
		}

		ArrayExtensions.arrayRemove(UnitsLocal, ent)
	}
})

Events.on("SharedObjectChanged", async (id, _, obj) => {
	if (id !== SOType.GameAccountClient || IsGameStarted)
		return
	await EventsX.emit("GameEnded", false)
	IsGameStarted = false
	IsGameStartedPlayer = false
})
