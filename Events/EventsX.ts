import { EventEmitter, Hero, Unit, Vector2 } from "wrapper/Imports"

export const EventsX: EventsX = new EventEmitter()

export interface EventsX extends EventEmitter {
	on(name: "WindowChange", callback: (new_state: Vector2, old_state?: Vector2) => void): EventEmitter
}

export interface EventsX extends EventEmitter {
	on(name: "removeControllable", callback: (unit: Unit) => void): EventEmitter
}

export interface EventsX extends EventEmitter {
	on(name: "GameStarted", callback: (localPlayer: Hero) => void): EventEmitter
}

export interface EventsX extends EventEmitter {
	on(name: "GameEnded", callback: () => void): EventEmitter
}
